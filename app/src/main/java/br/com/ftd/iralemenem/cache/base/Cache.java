package br.com.ftd.iralemenem.cache.base;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by ranipieper on 7/25/16.
 */
public interface Cache<T extends RealmObject> {

    T put(Realm realm, T t);

    List<T> putAll(Realm realm, List<T> t);

    void delete(Realm realm, String id);

    void delete(Realm realm, Long id);

    void deleteAll(Realm realm);

    T get(Realm realm, String id);

    T get(Realm realm, Long id);

    long count(Realm realm);

    void updateWithoutNullsOnlyExposeGson(Realm realm, T obj);
    void updateWithoutNullsOnlyExposeGson(Realm realm, List<T> lst);
    void updateWithoutNullsAllFields(Realm realm, T obj);
    void updateWithoutNullsAllFields(Realm realm, List<T> lst);


    String getPrimaryKeyName();
}