package br.com.ftd.iralemenem.service.response;

import com.google.gson.annotations.Expose;

import br.com.ftd.iralemenem.cache.base.SerializedGsonName;

/**
 * Created by ranipieper on 7/25/16.
 */
public class Pagination {
    /*
    "limit": 100,
    "offset": 0,
    "page_current": 1,
    "use": true,
    "page_total": 4,
    "count_total": 303,
    "count_page": 100
     */

    @Expose
    private int limit;
    @Expose
    private int offset;
    @Expose
    private boolean use;
    @Expose
    @SerializedGsonName("page_total")
    private int pageTotal;
    @Expose
    @SerializedGsonName("count_total")
    private int countTotal;
    @Expose
    @SerializedGsonName("countPage")
    private int countPage;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public boolean isUse() {
        return use;
    }

    public void setUse(boolean use) {
        this.use = use;
    }

    public int getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(int pageTotal) {
        this.pageTotal = pageTotal;
    }

    public int getCountTotal() {
        return countTotal;
    }

    public void setCountTotal(int countTotal) {
        this.countTotal = countTotal;
    }

    public int getCountPage() {
        return countPage;
    }

    public void setCountPage(int countPage) {
        this.countPage = countPage;
    }
}
