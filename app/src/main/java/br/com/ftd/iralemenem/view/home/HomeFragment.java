package br.com.ftd.iralemenem.view.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Date;

import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.cache.PlanningCacheManager;
import br.com.ftd.iralemenem.cache.TopicCacheManager;
import br.com.ftd.iralemenem.model.Planning;
import br.com.ftd.iralemenem.navigation.Navigator;
import br.com.ftd.iralemenem.util.DateUtil;
import br.com.ftd.iralemenem.util.NumberUtil;
import br.com.ftd.iralemenem.view.base.BaseFragment;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ranipieper on 7/7/16.
 */
public class HomeFragment extends BaseFragment {

    @BindView(R.id.txt_test_date)
    TextView mTxtTestDate;

    @BindView(R.id.txt_remaining_days)
    TextView mTxtRemainingDays;

    @BindView(R.id.txt_percent)
    TextView mTxtPercent;

    @BindView(R.id.progess)
    ProgressBar mProgress;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Planning planning = PlanningCacheManager.getInstance().get(getRealm(), Planning.ID);
        if (planning == null) {
            Navigator.navigateToChangeDateFragment(getContext());
            return;
        }

        mTxtTestDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(planning.getEndDate()));
        mTxtRemainingDays.setText(String.valueOf(DateUtil.getDiffDays(new Date(), planning.getEndDate())));

        int percent = getTopicsPercent();
        mTxtPercent.setText(getString(R.string.txt_percent, percent));
        mProgress.setProgress(getDatePercent(planning));
    }

    private int getDatePercent(Planning planning) {
        long totalDias = DateUtil.getDiffDays(planning.getInitDate(), planning.getEndDate());
        long totalDiasPassados = DateUtil.getDiffDays(planning.getInitDate(), new Date());

        return NumberUtil.getPercent(totalDiasPassados, totalDias);
    }

    private int getTopicsPercent() {
        long totalTopics = TopicCacheManager.getInstance().count(getRealm());
        long totalTopicsFinalized = TopicCacheManager.getInstance().countFinalized(getRealm());

        return NumberUtil.getPercent(totalTopicsFinalized, totalTopics);
    }

    @OnClick(R.id.btn_change_date)
    void btnChangeDateClick() {
        Navigator.navigateToChangeDateFragment(getContext());
    }

    @OnClick(R.id.btn_planning)
    void planningClick() {
        Navigator.navigateToPlanningFragment(getContext());
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.title_home);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }
}
