package br.com.ftd.iralemenem.view.base;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

import br.com.ftd.iralemenem.R;

public abstract class BaseFragmentActivity extends BaseActivity {

    protected List<String> mFragments = new ArrayList<>();

    protected abstract int getLayoutFragmentContainer();

    private boolean FADE_TRANSITION_DEFAULT = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param fragment The fragment to be added.
     */
    public void finishAndAddFragment(BaseFragment fragment) {
        addFragment(null, fragment, false, true, FADE_TRANSITION_DEFAULT);
    }

    public void addFragment(BaseFragment fragment) {
        addFragment(null, fragment, false, false, FADE_TRANSITION_DEFAULT);
    }

    public void addFragment(BaseFragment fragment, boolean clearBackStack, boolean fade) {
        addFragment(null, fragment, clearBackStack, false, fade);
    }

    public void addFragment(BaseFragment fragment, boolean clearBackStack) {
        addFragment(null, fragment, clearBackStack, false, FADE_TRANSITION_DEFAULT);
    }

    public void addFragment(BaseFragment actualFragment, BaseFragment fragment) {
        addFragment(actualFragment, fragment, false, false, FADE_TRANSITION_DEFAULT);
    }

    public void addFragment(BaseFragment actualFragment, BaseFragment fragment, boolean clearBackStack, boolean finishActualScreen, boolean fade) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (clearBackStack) {
            clearBackStack(transaction);
        } else if (finishActualScreen) {
            clearFirstBackStack(transaction);
        }

        if (fade) {
            transaction.setCustomAnimations(R.anim.screen_fade_in, R.anim.screen_fade_out);
        } else {
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                    R.anim.slide_in_left, R.anim.slide_out_right);
        }

        String tag = fragment.getClass().getCanonicalName();
        transaction.add(getLayoutFragmentContainer(), fragment, tag).addToBackStack(tag).commitAllowingStateLoss();

        if (actualFragment != null) {
            fragment.setTargetFragment(actualFragment, -1);
        }
        synchronized (mFragments) {
            mFragments.add(tag);
        }

        try {
            getSupportFragmentManager().executePendingTransactions();
        } catch(IllegalStateException e) {
            Crashlytics.logException(e);
        }

    }

    public Fragment getActualFragment() {
        if (mFragments.size() > 0) {
            FragmentManager manager = getSupportFragmentManager();
            return manager.findFragmentByTag(mFragments.get(mFragments.size() - 1));
        }

        return null;
    }


    public void clearFirstBackStack(FragmentTransaction transaction) {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            transaction.remove(getActualFragment());
        }
        if (mFragments.size() > 0) {
            mFragments.remove(mFragments.size() - 1);
        }

    }

    public void clearBackStack(FragmentTransaction transaction) {
        FragmentManager manager = getSupportFragmentManager();
        for (String fragmentTag : mFragments) {
            manager.findFragmentByTag(fragmentTag);
            transaction.remove(getActualFragment());
        }
        mFragments = new ArrayList<>();
    }

    @Override
    public void onBackPressed() {
        boolean processBackPressed = true;
        if (getActualFragment() != null) {
            if (getActualFragment() instanceof BaseFragment) {
                processBackPressed = ((BaseFragment) getActualFragment()).backButtonNavigateToPreviousFragment();
            }
        }
        if (processBackPressed) {
            if (mFragments.size() <= 1) {
                finish();
            } else {
                callFragmentBackPressed();
                super.onBackPressed();
            }
        }
    }

    protected void callFragmentBackPressed() {
        FragmentManager manager = getSupportFragmentManager();

        if (manager != null) {
            synchronized (mFragments) {
                if (mFragments.size() > 0) {
                    Fragment lastFragment = manager.findFragmentByTag(mFragments.get(mFragments.size() - 1));
                    if (lastFragment != null && lastFragment instanceof BaseFragment) {
                        ((BaseFragment) lastFragment).onBackPressed();
                    }

                    Fragment prevFragment = null;
                    //previous fragment
                    if (mFragments.size() > 1) {
                        prevFragment = manager.findFragmentByTag(mFragments.get(mFragments.size() - 2));
                    }

                    //remove o fragment
                    mFragments.remove(mFragments.size() - 1);

                    if (prevFragment != null && prevFragment instanceof BaseFragment) {
                        ((BaseFragment) prevFragment).onResumeFromBackStack();
                    }
                }
            }
        }
    }

    public void setNavBackMode() {
        setNavBackMode(getDrawableBackMode(), getDrawableBackModeListener());
    }

    protected Drawable getDrawableBackMode() {
        return getDrawerToggleDelegate().getThemeUpIndicator();
    }

    protected View.OnClickListener getDrawableBackModeListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        };
    }

    public void setNavBackMode(Drawable drawable, View.OnClickListener listener) {
        if (mToolbar != null) {
            mToolbar.setVisibility(View.VISIBLE);
            mToolbar.setNavigationOnClickListener(listener);
            mToolbar.setNavigationIcon(drawable);
        }
    }

    public void setToolbarInvisible() {
        if (mToolbar != null) {
            mToolbar.setVisibility(View.GONE);
        }
    }

    public void resetBackMode() {
        if (mToolbar != null) {
            mToolbar.setVisibility(View.VISIBLE);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
    }

    public void hideDrawerAndBackButton() {
        if (mToolbar != null) {
            mToolbar.setVisibility(View.VISIBLE);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            mToolbar.setNavigationIcon(null);
            //mToolbar.setNavigationIcon(android.R.color.transparent);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getActualFragment() != null) {
            getActualFragment().onActivityResult(requestCode, resultCode, data);
        }
    }

    public int countFragments() {
        synchronized (mFragments) {
            return mFragments.size();
        }
    }
}
