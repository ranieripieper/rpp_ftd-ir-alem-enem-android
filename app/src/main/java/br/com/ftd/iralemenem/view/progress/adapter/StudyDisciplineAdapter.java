package br.com.ftd.iralemenem.view.progress.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.model.Discipline;

/**
 * Created by ranipieper on 7/4/16.
 */
public class StudyDisciplineAdapter extends RecyclerView.Adapter<StudyDisciplineAdapter.ViewHolder> {
    private List<Discipline> mDataset;
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtSubject;
        public TextView mTxtPecent;
        public TextView mTxtQuestions;
        public ProgressBar mProgressBar;

        public ViewHolder(View v) {
            super(v);
            mTxtSubject = (TextView) v.findViewById(R.id.txt_subject);
            mTxtPecent = (TextView) v.findViewById(R.id.txt_percent);
            mTxtQuestions = (TextView) v.findViewById(R.id.txt_questions);
            mProgressBar = (ProgressBar) v.findViewById(R.id.progess);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public StudyDisciplineAdapter(Context context, List<Discipline> myDataset) {
        mContext = context;
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public StudyDisciplineAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_practice_test, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Discipline obj = mDataset.get(position);

        holder.mTxtSubject.setText(obj.getDiscipline());
        holder.mTxtQuestions.setVisibility(View.GONE);

        holder.mTxtPecent.setText(mContext.getString(R.string.txt_percent, (int)obj.getPercent()));
        holder.mProgressBar.setProgress(Double.valueOf(obj.getPercent()).intValue());

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void addData(List data) {
        if (null == data || data.isEmpty()) {
            return;
        }

        int startPosition = getItemCount();
        mDataset.addAll(data);

        notifyItemRangeInserted(startPosition, getItemCount());
    }

}