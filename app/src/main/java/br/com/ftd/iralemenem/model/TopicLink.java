package br.com.ftd.iralemenem.model;

import com.google.gson.annotations.Expose;

import br.com.ftd.iralemenem.cache.base.SerializedGsonName;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranieripieper on 8/1/16.
 */
public class TopicLink extends RealmObject {

    @Expose
    @PrimaryKey
    private long id;

    @Expose
    private String description;

    @Expose
    private String link;

    @Expose
    @SerializedGsonName("topic_id")
    private long topicId;

    @Expose
    @SerializedGsonName("content_type_id")
    private long contentTypeId;


    /**
     * Gets the id
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets the description
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the link
     *
     * @return link
     */
    public String getLink() {
        return link;
    }

    /**
     * Sets the link
     *
     * @param link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * Gets the topicId
     *
     * @return topicId
     */
    public long getTopicId() {
        return topicId;
    }

    /**
     * Sets the topicId
     *
     * @param topicId
     */
    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    /**
     * Gets the contentTypeId
     *
     * @return contentTypeId
     */
    public long getContentTypeId() {
        return contentTypeId;
    }

    /**
     * Sets the contentTypeId
     *
     * @param contentTypeId
     */
    public void setContentTypeId(long contentTypeId) {
        this.contentTypeId = contentTypeId;
    }
}
