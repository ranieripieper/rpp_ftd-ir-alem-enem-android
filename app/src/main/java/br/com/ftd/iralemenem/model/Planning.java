package br.com.ftd.iralemenem.model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranipieper on 7/25/16.
 */
public class Planning extends RealmObject {

    public static final long ID = 1;

    @PrimaryKey
    private long id = ID;
    private Date initDate;
    private Date endDate;

    private RealmList<PlanningWeek> planningWeek;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public RealmList<PlanningWeek> getPlanningWeek() {
        return planningWeek;
    }

    public void setPlanningWeek(RealmList<PlanningWeek> planningWeek) {
        this.planningWeek = planningWeek;
    }
}
