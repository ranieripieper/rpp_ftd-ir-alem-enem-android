package br.com.ftd.iralemenem.view.account;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import br.com.ftd.iralemenem.BuildConfig;
import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.model.User;
import br.com.ftd.iralemenem.navigation.Navigator;
import br.com.ftd.iralemenem.service.RetrofitManager;
import br.com.ftd.iralemenem.service.response.LoginResponse;
import br.com.ftd.iralemenem.util.SharedPrefManager;
import br.com.ftd.iralemenem.view.base.BaseFragment;
import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 7/4/16.
 */
public class LoginFragment extends BaseFragment {

    @BindView(R.id.txt_erros)
    TextView mTxtErros;

    @BindView(R.id.txt_forgot_password)
    TextView mTxtForgotPwd;

    @Email(messageResId = R.string.invalid_email)
    @BindView(R.id.edt_email)
    EditText mEdtEmail;

    @Password(min = 6, scheme = Password.Scheme.ANY, messageResId = R.string.invalid_password)
    @BindView(R.id.edt_password)
    EditText mEdtPwd;

    @BindView(R.id.img_input_email)
    ImageView mImgEmail;
    @BindView(R.id.img_input_pwd)
    ImageView mImgPwd;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();

        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTxtForgotPwd.setPaintFlags(mTxtForgotPwd.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if (BuildConfig.DEBUG) {
            mEdtEmail.setText("douglas.miguel@gmail.com");
            mEdtPwd.setText("ftd1902");
        }
    }

    private void callService(){
        showLoading();
        Call<LoginResponse> service = RetrofitManager.getInstance().getUserService().login(mEdtEmail.getText().toString(), mEdtPwd.getText().toString(), BuildConfig.FTD_APPLICATION_ID);
        service.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                dismissLoading();
                if (response != null && response.body() != null) {
                    processLogin(response.body());
                } else {
                    showError(getString(R.string.generic_error));
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                showError(getString(R.string.generic_error));
            }
        });

    }

    private void processLogin(LoginResponse response) {
        //tenta fazer o parse de Login com sucesso
        Gson gson = RetrofitManager.getGson();
        try {
            User user = gson.fromJson(response.getData(), User.class);
            if (TextUtils.isEmpty(user.getToken())) {
                showError();
            } else {
                SharedPrefManager.getInstance().setToken(user.getToken());
                Navigator.navigateToPracticeTestProgressFragment(getContext());
            }
        } catch (JsonSyntaxException e) {
            //deu algum erro. Tenta recuperar o erro
            if (response.getError().isString()) {
                showLoginError(response.getError().getAsString());
            } else {
                showError();
            }
        }
    }

    private void showLoginError(String error) {
        if (LoginResponse.INVALID_PASSWORD.equalsIgnoreCase(error)) {
            showError(getString(R.string.login_invalid_password));
        } else if (LoginResponse.USER_NOT_FOUND.equalsIgnoreCase(error)) {
            showError(getString(R.string.login_user_not_found));
        } else {
            showError();
        }
    }

    @Override
    public void onValidationSucceeded() {
        callService();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isAdded()) {
            String txtError = "";
            for (ValidationError error : errors) {
                View view = error.getView();
                String message = error.getCollatedErrorMessage(getContext());
                txtError += message + "\n";

                if (view.getId() == R.id.edt_email) {
                    mImgEmail.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImageError, null));
                } else if (view.getId() == R.id.edt_password) {
                    mImgPwd.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImageError, null));
                }
            }
            showError(txtError);

        }
    }

    protected void showError() {
        showError(getString(R.string.generic_error));
    }

    protected void showError(String txtError) {
        if (!isAdded()) {
            return;
        }
        mTxtErros.setText(txtError);
        mTxtErros.setVisibility(View.VISIBLE);
        dismissLoading();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_login;
    }

    @OnClick(R.id.btn_submit)
    void submitClick() {
        clearValidation();
        initValidator();
        mValidator.validate();
    }


    @OnClick(R.id.txt_forgot_password)
    void forgotPasswordClick() {
        TODO();
    }

    @OnClick(R.id.btn_sign_up)
    void signUpClick() {
        Navigator.navigateToCreateAccountFragment(getContext());
    }

    private void clearValidation() {
        mTxtErros.setVisibility(View.GONE);
        mTxtErros.setText("");
        mImgEmail.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImage, null));
        mImgPwd.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImage, null));
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.title_login);
    }
}
