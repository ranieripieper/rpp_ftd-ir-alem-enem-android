package br.com.ftd.iralemenem.view.planning;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import antistatic.spinnerwheel.AbstractWheel;
import antistatic.spinnerwheel.OnWheelClickedListener;
import antistatic.spinnerwheel.OnWheelScrollListener;
import antistatic.spinnerwheel.WheelHorizontalView;
import br.com.ftd.iralemenem.BuildConfig;
import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.cache.DisciplineCacheManager;
import br.com.ftd.iralemenem.cache.PlanningCacheManager;
import br.com.ftd.iralemenem.cache.PlanningWeekCacheManager;
import br.com.ftd.iralemenem.cache.TopicCacheManager;
import br.com.ftd.iralemenem.model.Discipline;
import br.com.ftd.iralemenem.model.Planning;
import br.com.ftd.iralemenem.model.PlanningWeek;
import br.com.ftd.iralemenem.model.Topic;
import br.com.ftd.iralemenem.navigation.Navigator;
import br.com.ftd.iralemenem.util.DateUtil;
import br.com.ftd.iralemenem.view.base.BaseFragment;
import br.com.ftd.iralemenem.view.custom.DividerDecoration;
import br.com.ftd.iralemenem.view.planning.adapter.PlanningAdapterListener;
import br.com.ftd.iralemenem.view.planning.adapter.PlanningSection;
import br.com.ftd.iralemenem.view.planning.adapter.WeeksAdapter;
import br.com.ftd.iralemenem.view.planning.adapter.WheelTopicAdapter;
import butterknife.BindView;
import butterknife.OnClick;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.realm.Realm;

/**
 * Created by ranipieper on 7/26/16.
 */
public class PlanningFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.wheel_weeks)
    WheelHorizontalView mWheelWeeks;
    private LinearLayoutManager mLinearLayoutManager;

    @BindView(R.id.txt_date)
    TextView txtDate;

    @BindView(R.id.btn_next)
    View mBtnNext;

    @BindView(R.id.btn_previous)
    View mBtnPrevious;

    private SectionedRecyclerViewAdapter mSectionAdapter;

    private int mSelectedWeek = -1;
    private WeeksAdapter mWeekAdapter;

    public static PlanningFragment newInstance() {
        PlanningFragment fragment = new PlanningFragment();
        return fragment;
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        changeWeek(mSelectedWeek);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Planning planning = PlanningCacheManager.getInstance().get(getRealm(), Planning.ID);
        if (planning == null) {
            Navigator.navigateToChangeDateFragment(getContext());
            return;
        }

        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mWeekAdapter = new WeeksAdapter(getActivity(), planning.getPlanningWeek().size());

        mRecyclerView.addItemDecoration(new DividerDecoration.Builder(getContext()).setHeight(R.dimen.default_divider_height)
                .setItemPadding(R.dimen.divider_padding)
                .setColorResource(R.color.gray)
                .build());
        mWheelWeeks.setVisibleItems(3);
        mWheelWeeks.setViewAdapter(mWeekAdapter);

        mWheelWeeks.addScrollingListener(new OnWheelScrollListener() {
            @Override
            public void onScrollingStarted(AbstractWheel abstractWheel) {
            }

            @Override
            public void onScrollingFinished(AbstractWheel abstractWheel) {
                int newValue = mWeekAdapter.getValue(abstractWheel.getCurrentItem());
                if (mSelectedWeek != newValue) {
                    changeWeek(newValue);
                }
            }
        });

        mWheelWeeks.addClickingListener(new OnWheelClickedListener() {
            @Override
            public void onItemClicked(AbstractWheel abstractWheel, int i) {
                if (i != abstractWheel.getCurrentItem()) {
                    abstractWheel.setCurrentItem(i, true);
                }
            }
        });

        mWheelWeeks.setCurrentItem(planning.getPlanningWeek().size() > 3 ? 3 : planning.getPlanningWeek().size());

        mWheelWeeks.postDelayed(new Runnable() {
            @Override
            public void run() {
                mWheelWeeks.setCurrentItem(0, true);
                changeWeek(1);
            }
        }, 100);
    }

    private void changeWeek(int week) {
        mSelectedWeek = week;

        if (mSelectedWeek == 1) {
            mBtnPrevious.setVisibility(View.INVISIBLE);
        } else {
            mBtnPrevious.setVisibility(View.VISIBLE);
        }
        if (mSelectedWeek >= mWeekAdapter.getItemsCount()) {
            mBtnNext.setVisibility(View.INVISIBLE);
        } else {
            mBtnNext.setVisibility(View.VISIBLE);
        }

        PlanningWeek planningWeek = PlanningWeekCacheManager.getInstance().get(getRealm(), Long.valueOf(mSelectedWeek));

        List<Topic> lst = new ArrayList<>();
        if (planningWeek != null) {
            if (planningWeek.getStart() != null) {
                txtDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(planningWeek.getStart()));
            } else {
                txtDate.setText("");
            }

            lst = planningWeek.getTopics();

            Collections.sort(lst, new Comparator<Topic>() {
                @Override
                public int compare(Topic topic, Topic t1) {
                    return topic.getDisciplineId().compareTo(t1.getDisciplineId());
                }
            });
        }


        PlanningAdapterListener listener = new PlanningAdapterListener() {
            @Override
            public void onTopicClicked(Topic topic) {
                Navigator.navigateToTopicDetailFragment(getActivity(), topic.getId());
            }

            @Override
            public void addTopicClick(long disciplineId, int week) {
                addTopicToWeek(disciplineId, week);
            }

            @Override
            public void makeTestClick(long disciplineId, int week, List<Topic> topics) {
                makeTestWeek(disciplineId, week, topics);
            }

            @Override
            public void onTopicDoneClicked(Topic topic, int position) {
                topic.setFinalized(!topic.isFinalized());
                Realm realm = Realm.getDefaultInstance();
                TopicCacheManager.getInstance().put(realm, topic);
                realm.close();
                if (mSectionAdapter != null && isAdded()) {
                    //mSectionAdapter.notifyItemChanged(position);
                }
            }
        };

        mSectionAdapter = new SectionedRecyclerViewAdapter();
        long disciplineId = -1;
        int section = 1;
        List<Topic> topicsDiscipline = new ArrayList<>();
        if (lst != null && !lst.isEmpty()) {
            for (Topic topic : lst) {
                if (disciplineId != topic.getDisciplineId().longValue() && disciplineId != -1) {
                    Discipline discipline = DisciplineCacheManager.getInstance().get(getRealm(), disciplineId);
                    mSectionAdapter.addSection(new PlanningSection(getActivity(), section, discipline, topicsDiscipline, mSelectedWeek, listener));
                    section += topicsDiscipline.size() + 1;
                    topicsDiscipline = new ArrayList<>();
                }
                topicsDiscipline.add(topic);
                disciplineId = topic.getDisciplineId();
            }
            Discipline discipline = DisciplineCacheManager.getInstance().get(getRealm(), disciplineId);
            mSectionAdapter.addSection(new PlanningSection(getActivity(), section++, discipline, topicsDiscipline, mSelectedWeek, listener));
        }

        List<Discipline> disciplesWithoutTopics = getDisciplesWithoutTopics(lst);
        if (disciplesWithoutTopics != null && !disciplesWithoutTopics.isEmpty()) {
            for (Discipline d : disciplesWithoutTopics) {
                mSectionAdapter.addSection(new PlanningSection(getActivity(), section++, d, new ArrayList<Topic>(), mSelectedWeek, listener));
            }
        }

        mRecyclerView.setAdapter(mSectionAdapter);

    }

    private List<Discipline> getDisciplesWithoutTopics(List<Topic> lst) {
        List<Discipline> result = new ArrayList<>();

        List<Discipline> disciplines = DisciplineCacheManager.getInstance().getAll(getRealm());

        if (disciplines != null) {
            disciplinesFor : for (Discipline discipline : disciplines) {
                if (TopicCacheManager.getInstance().count(getRealm(), discipline.getId()) > 0) {
                    //verifica se a disciplina está na lista de tópicos
                    for (Topic topic : lst) {
                        if ((topic.getDisciplineId() != null && discipline.getId() == topic.getDisciplineId()) ||
                                (topic.getDiscipline() != null && topic.getDiscipline().getId() == discipline.getId())) {
                            continue disciplinesFor;
                        }
                    }
                    //adiciona tópico fake
                    result.add(discipline);
                }
            }
        }

        return result;
    }

/*
    private List<Topic> getDisciplesWithoutTopics1(List<Topic> lst) {
        List<Discipline> disciplines = DisciplineCacheManager.getInstance().getAll(getRealm());

        if (disciplines != null) {
            disciplinesFor : for (Discipline discipline : disciplines) {
                if (TopicCacheManager.getInstance().count(getRealm(), discipline.getId()) > 0) {
                    //verifica se a disciplina está na lista de tópicos
                    for (Topic topic : lst) {
                        if ((topic.getDisciplineId() != null && discipline.getId() == topic.getDisciplineId()) ||
                                (topic.getDiscipline() != null && topic.getDiscipline().getId() == topic.getDisciplineId())) {
                            continue disciplinesFor;
                        }
                    }
                    //adiciona tópico fake
                    lst.add(Topic.getFake(discipline.getId()));
                }
            }
        }

        return lst;
    }
*/
    private void makeTestWeek(long disciplineId, int week, List<Topic> lstTopic) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        String topics = "";
        for (Topic topic : lstTopic) {
            topics += topic.getId() + ",";
        }
        if (!TextUtils.isEmpty(topics)) {
            topics = topics.substring(0, topics.length() - 1);
        }
        String url = String.format(BuildConfig.URL_SIMULADOS, disciplineId, topics);
        intent.setData(Uri.parse(url));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.error_download_app_simulados)
                    .setPositiveButton(R.string.install, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.URL_SIMULADOS_GOOGLE_PLAY));
                            startActivity(browserIntent);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });

            builder.create().show();
        }
    }

    private void addTopicToWeek(long disciplineId, int week) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_topic, null);

        final List<Topic> lstTopics = TopicCacheManager.getInstance().getAll(getRealm(), disciplineId);
        final br.com.ftd.iralemenem.view.custom.WheelVerticalView mWheel = (br.com.ftd.iralemenem.view.custom.WheelVerticalView) view.findViewById(R.id.wheel_topics);
        final WheelTopicAdapter adapter = new WheelTopicAdapter(getActivity(), lstTopics);
        mWheel.setViewAdapter(adapter);
        builder
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Topic topic = lstTopics.get(mWheel.getCurrentItem());
                        PlanningWeek planningWeek = PlanningWeekCacheManager.getInstance().get(getRealm(), Long.valueOf(mSelectedWeek));
                        planningWeek.getTopics().add(topic);
                        PlanningWeekCacheManager.getInstance().put(getRealm(), planningWeek);
                        int firstPosition = mLinearLayoutManager.findFirstVisibleItemPosition();
                        changeWeek(mSelectedWeek);
                        mRecyclerView.scrollToPosition(firstPosition);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        builder.setView(view);
        mWheel.setCurrentItem(lstTopics.size() / 2);
        mWheel.setVisibleItems(7);
        builder.create().show();
    }

    @OnClick(R.id.btn_next)
    void btnNextClick() {
        if (mSelectedWeek >= mWeekAdapter.getItemsCount()) {
            return;
        }
        int newIndex = mSelectedWeek;
        mWheelWeeks.setCurrentItem(newIndex, true);
    }

    @OnClick(R.id.btn_previous)
    void btnPreviousClick() {
        if (mSelectedWeek < 2) {
            return;
        }
        int newIndex = mSelectedWeek - 2;
        mWheelWeeks.setCurrentItem(newIndex, true);
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.title_planning);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_planning;
    }
}
