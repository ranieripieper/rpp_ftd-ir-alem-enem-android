package br.com.ftd.iralemenem.cache.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by ranipieper on 7/25/16.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SerializedGsonName {
    /**
     * @return the desired name of the field when it is serialized
     */
    String value();
}
