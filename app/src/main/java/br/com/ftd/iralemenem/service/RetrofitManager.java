package br.com.ftd.iralemenem.service;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import br.com.ftd.iralemenem.BuildConfig;
import br.com.ftd.iralemenem.cache.base.SerializedGsonName;
import br.com.ftd.iralemenem.service.interfaces.PerformanceService;
import br.com.ftd.iralemenem.service.interfaces.TopicService;
import br.com.ftd.iralemenem.service.interfaces.UserService;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ranipieper on 7/25/16.
 */
public class RetrofitManager {

    public static int CONNECT_TIMEOUT = 300;
    public static int READ_TIMEOUT = 300;
    public static int WRITE_TIMEOUT = 300;

    public Retrofit mRetrofitApi;
    final ConcurrentHashMap<Class, Object> services;
    private static RetrofitManager instance;

    private RetrofitManager() {
        services = new ConcurrentHashMap();
    }

    public static RetrofitManager getInstance() {
        if (instance == null) {
            instance = new RetrofitManager();
        }
        return instance;
    }

    public static Gson getGson() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .setFieldNamingStrategy(new FieldNamingStrategy() {
                    @Override
                    public String translateName(Field field) {
                        if (field.isAnnotationPresent(SerializedGsonName.class)) {
                            SerializedGsonName serializedName = field.getAnnotation(SerializedGsonName.class);
                            return serializedName.value();
                        }
                        return field.getName();
                    }
                })
                .create();
        return gson;
    }

    public void initialize() {
        configApi();
    }

    private void configApi() {
        //HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS);
        httpClient.writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);
        httpClient.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);
        //httpClient.addInterceptor(logging);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                /*
                Request request = chain.request();
                request = request.newBuilder().build();
                Response response = chain.proceed(request);
                return response;
                */
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("token", BuildConfig.TOKEN)
                        .build();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);

            }
        });

        mRetrofitApi = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .client(httpClient.build())
                .build();

    }

    //Services
    public TopicService getTopicService() {
        return (TopicService)this.getServiceApi(TopicService.class);
    }

    public UserService getUserService() {
        return (UserService)this.getServiceApi(UserService.class);
    }

    public PerformanceService getPerformanceService() {
        return (PerformanceService)this.getServiceApi(PerformanceService.class);
    }

    protected <T> Object getServiceApi(Class<T> cls) {
        if(!this.services.contains(cls)) {
            this.services.putIfAbsent(cls, this.mRetrofitApi.create(cls));
        }

        return this.services.get(cls);
    }
}