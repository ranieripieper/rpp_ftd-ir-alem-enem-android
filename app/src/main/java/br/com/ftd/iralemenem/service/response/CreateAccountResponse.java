package br.com.ftd.iralemenem.service.response;

/**
 * Created by ranieripieper on 8/2/16.
 */
public class CreateAccountResponse extends JsonResponse {

    public static final String USER_ALREADY_EXISTS = "user-already-exists";

}
