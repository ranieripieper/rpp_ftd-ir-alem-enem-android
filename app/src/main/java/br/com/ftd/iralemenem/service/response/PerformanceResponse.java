package br.com.ftd.iralemenem.service.response;

import br.com.ftd.iralemenem.model.Performance;

/**
 * Created by ranipieper on 8/4/16.
 */
public class PerformanceResponse extends BaseListResponse<Performance> {

    public static final String INVALID_SESSION = "invalid-session";
}
