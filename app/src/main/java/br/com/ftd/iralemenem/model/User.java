package br.com.ftd.iralemenem.model;

import com.google.gson.annotations.Expose;

import br.com.ftd.iralemenem.cache.base.SerializedGsonName;

/**
 * Created by ranieripieper on 8/2/16.
 */
public class User {

    @Expose
    @SerializedGsonName("user_id")
    private long userId;

    @Expose
    @SerializedGsonName("person_id")
    private long personId;

    @Expose
    @SerializedGsonName("email")
    private String email;

    @Expose
    @SerializedGsonName("name")
    private String name;

    @Expose
    @SerializedGsonName("token")
    private String token;

    /**
     * Gets the userId
     *
     * @return userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Sets the userId
     *
     * @param userId
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Gets the personId
     *
     * @return personId
     */
    public long getPersonId() {
        return personId;
    }

    /**
     * Sets the personId
     *
     * @param personId
     */
    public void setPersonId(long personId) {
        this.personId = personId;
    }

    /**
     * Gets the email
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
