package br.com.ftd.iralemenem.navigation;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import br.com.ftd.iralemenem.util.SharedPrefManager;
import br.com.ftd.iralemenem.view.MainActivity;
import br.com.ftd.iralemenem.view.base.BaseFragmentActivity;
import br.com.ftd.iralemenem.view.account.CreateAccountFragment;
import br.com.ftd.iralemenem.view.account.LoginFragment;
import br.com.ftd.iralemenem.view.config.ChangeDateFragment;
import br.com.ftd.iralemenem.view.home.HomeFragment;
import br.com.ftd.iralemenem.view.planning.PlanningFragment;
import br.com.ftd.iralemenem.view.planning.TopicDetailFragment;
import br.com.ftd.iralemenem.view.progress.PracticeTestProgressFragment;
import br.com.ftd.iralemenem.view.progress.StudyProgressFragment;

/**
 * Created by ranipieper on 7/4/16.
 */
public class Navigator {

    public static void navigateToMain(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, MainActivity.class));
        }
    }

    public static void navigateToCreateAccountFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(CreateAccountFragment.newInstance(), false);
        }
    }

    public static void navigateToLoginFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(LoginFragment.newInstance(), true);
        }
    }

    public static void navigateToHomeFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(HomeFragment.newInstance(), true);
        }
    }

    public static void navigateToPracticeTestProgressFragment(Context context) {
        if (context != null) {
            //verifica se está logado
            String token = SharedPrefManager.getInstance().getToken();
            if (TextUtils.isEmpty(token)) {
                navigateToLoginFragment(context);
            } else {
                ((BaseFragmentActivity) context).addFragment(PracticeTestProgressFragment.newInstance(), true);
            }
        }
    }

    public static void navigateToChangeDateFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(ChangeDateFragment.newInstance(), true);
        }
    }

    public static void navigateToPlanningFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(PlanningFragment.newInstance(), true);
        }
    }

    public static void navigateToTopicDetailFragment(Context context, long topicId) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(TopicDetailFragment.newInstance(topicId), false);
        }
    }

    public static void navigateToStudyProgressFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(StudyProgressFragment.newInstance(), true);
        }
    }


}
