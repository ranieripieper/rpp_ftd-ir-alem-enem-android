package br.com.ftd.iralemenem.service.interfaces;

import br.com.ftd.iralemenem.service.response.CreateAccountResponse;
import br.com.ftd.iralemenem.service.response.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ranipieper on 7/25/16.
 */
public interface UserService {

    @POST("simapp/user")
    @FormUrlEncoded
    Call<CreateAccountResponse> createAccount(@Field("email") String email, @Field("password") String password, @Field("name") String name);

    @POST("login")
    @FormUrlEncoded
    Call<LoginResponse> login(@Field("username") String email, @Field("password") String password, @Field("application_id") int applicationId);

}
