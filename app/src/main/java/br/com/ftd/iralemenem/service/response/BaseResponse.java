package br.com.ftd.iralemenem.service.response;

import com.google.gson.annotations.Expose;

/**
 * Created by ranipieper on 7/25/16.
 */
public class BaseResponse<T> {

    @Expose
    private Pagination pagination;

    @Expose
    private T data;

    @Expose
    private boolean error;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
