package br.com.ftd.iralemenem.view.progress.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.model.ExamsPerformance;
import br.com.ftd.iralemenem.model.Performance;
import br.com.ftd.iralemenem.util.NumberUtil;

/**
 * Created by ranipieper on 7/4/16.
 */
public class PracticeTestAdapter extends RecyclerView.Adapter<PracticeTestAdapter.ViewHolder> {
    private List<Performance> mDataset;
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtSubject;
        public TextView mTxtPecent;
        public TextView mTxtQuestions;
        public ProgressBar mProgressBar;

        public ViewHolder(View v) {
            super(v);
            mTxtSubject = (TextView) v.findViewById(R.id.txt_subject);
            mTxtPecent = (TextView) v.findViewById(R.id.txt_percent);
            mTxtQuestions = (TextView) v.findViewById(R.id.txt_questions);
            mProgressBar = (ProgressBar) v.findViewById(R.id.progess);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PracticeTestAdapter(Context context, List<Performance> myDataset) {
        mContext = context;
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PracticeTestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_practice_test, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Performance obj = mDataset.get(position);

        holder.mTxtSubject.setText(obj.getDiscipline());

        ExamsPerformance examsPerformance = new ExamsPerformance();
        if (obj.getExamsPerformance() != null && !obj.getExamsPerformance().isEmpty()) {
            for (ExamsPerformance tmp : obj.getExamsPerformance()) {
                examsPerformance.setCorrectAnswers(examsPerformance.getCorrectAnswers() + tmp.getCorrectAnswers());
                examsPerformance.setTotalAnwsers(examsPerformance.getTotalAnwsers() + tmp.getTotalAnwsers());
            }
        }
        holder.mTxtQuestions.setText(mContext.getString(R.string.x_questions_of_y, examsPerformance.getCorrectAnswers(), examsPerformance.getTotalAnwsers()));
        int percent = NumberUtil.getPercent(examsPerformance.getCorrectAnswers(), examsPerformance.getTotalAnwsers());
        holder.mTxtPecent.setText(mContext.getString(R.string.txt_percent, percent));
        holder.mProgressBar.setProgress(percent);

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void addData(List data) {
        if (null == data || data.isEmpty()) {
            return;
        }

        int startPosition = getItemCount();
        mDataset.addAll(data);

        notifyItemRangeInserted(startPosition, getItemCount());
    }

}