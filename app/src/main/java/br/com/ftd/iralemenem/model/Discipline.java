package br.com.ftd.iralemenem.model;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranipieper on 7/25/16.
 */
public class Discipline extends RealmObject {

    @PrimaryKey
    @Expose
    private long id;

    @Expose
    private String discipline;

    @Expose
    private String code;

    private int percent;

    public Discipline() {
    }

    public Discipline(long id, String discipline) {
        this.id = id;
        this.discipline = discipline;
    }

    public Discipline(long id, String discipline, String code) {
        this.id = id;
        this.discipline = discipline;
        this.code = code;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the percent
     *
     * @return percent
     */
    public int getPercent() {
        return percent;
    }

    /**
     * Sets the percent
     *
     * @param percent
     */
    public void setPercent(int percent) {
        this.percent = percent;
    }
}
