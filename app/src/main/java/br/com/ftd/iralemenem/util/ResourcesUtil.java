package br.com.ftd.iralemenem.util;

import android.content.Context;

/**
 * Created by ranipieper on 7/26/16.
 */
public class ResourcesUtil {

    public static int getDrawableId(String name, Context context) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }
}
