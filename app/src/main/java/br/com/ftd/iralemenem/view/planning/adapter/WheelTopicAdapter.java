package br.com.ftd.iralemenem.view.planning.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import antistatic.spinnerwheel.adapters.AbstractWheelTextAdapter;
import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.model.Topic;

/**
 * Created by ranipieper on 7/26/16.
 */
public class WheelTopicAdapter extends AbstractWheelTextAdapter {

    private List<Topic> mTopics = new ArrayList<>();

    /**
     * Constructor
     */
    public WheelTopicAdapter(Context context, List<Topic> topics) {
        super(context, R.layout.row_wheel_topic, NO_RESOURCE);
        mTopics = topics;
        setItemTextResource(R.id.txt_topic);
    }

    @Override
    public View getItem(int index, View cachedView, ViewGroup parent) {
        View view = super.getItem(index, cachedView, parent);

        return view;
    }

    @Override
    public int getItemsCount() {
        return mTopics.size();
    }

    public long getValue(int index) {
        return mTopics.get(index).getId();
    }

    @Override
    protected CharSequence getItemText(int index) {
        return mTopics.get(index).getTopic();
    }

}
