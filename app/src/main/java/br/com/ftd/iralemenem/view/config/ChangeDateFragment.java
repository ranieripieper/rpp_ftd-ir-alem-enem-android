package br.com.ftd.iralemenem.view.config;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.QuickRule;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.ftd.iralemenem.BuildConfig;
import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.cache.PlanningCacheManager;
import br.com.ftd.iralemenem.cache.PlanningWeekCacheManager;
import br.com.ftd.iralemenem.cache.TopicCacheManager;
import br.com.ftd.iralemenem.model.Planning;
import br.com.ftd.iralemenem.model.PlanningWeek;
import br.com.ftd.iralemenem.model.Topic;
import br.com.ftd.iralemenem.navigation.Navigator;
import br.com.ftd.iralemenem.service.RetrofitManager;
import br.com.ftd.iralemenem.service.response.TopicsResponse;
import br.com.ftd.iralemenem.util.DateUtil;
import br.com.ftd.iralemenem.view.base.BaseFragment;
import butterknife.BindView;
import butterknife.OnClick;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 7/4/16.
 */
public class ChangeDateFragment extends BaseFragment {

    @BindView(R.id.txt_erros)
    TextView mTxtErros;

    @BindView(R.id.img_input_init_date)
    ImageView mImgInitDate;

    @BindView(R.id.img_input_end_date)
    ImageView mImgEndDate;

    @BindView(R.id.edt_init_date)
    @NotEmpty(messageResId = R.string.required_field_init_date)
    EditText mEdtInitDate;

    @BindView(R.id.edt_end_date)
    @NotEmpty(messageResId = R.string.required_field_end_date)
    EditText mEdtEndDate;

    public static ChangeDateFragment newInstance() {
        ChangeDateFragment fragment = new ChangeDateFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Planning planning = PlanningCacheManager.getInstance().get(getRealm());
        if (planning != null) {
            if (planning.getInitDate() != null) {
                mEdtInitDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(planning.getInitDate()));
            }
            if (planning.getEndDate() != null) {
                mEdtEndDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(planning.getEndDate()));
            }
        }
    }

    @OnClick({R.id.edt_init_date, R.id.img_input_init_date})
    void initDateClick() {
        if (isAdded()) {
            buildDatePicker(mEdtInitDate, null, null, new Date(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    clearValidation();
                }
            }).show();
        }
    }

    @OnClick({R.id.edt_end_date, R.id.img_input_end_date})
    void endDateClick() {
        if (isAdded()) {
            buildDatePicker(mEdtEndDate, null, new Date(), new Date(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    clearValidation();
                }
            }).show();
        }
    }

    @OnClick(R.id.btn_submit)
    void submitClick() {
        clearValidation();
        mValidator = null;
        initValidator();
        mValidator.put(mEdtEndDate, new QuickRule<EditText>() {
            @Override
            public boolean isValid(EditText view) {
                String initDate = mEdtInitDate.getText().toString();
                String endDate = mEdtEndDate.getText().toString();
                if (!TextUtils.isEmpty(initDate) && !TextUtils.isEmpty(endDate)) {
                    try {
                        Date dateInit = DateUtil.DATE_DAY_MONTH_YEAR.get().parse(initDate);
                        Date dateEnd = DateUtil.DATE_DAY_MONTH_YEAR.get().parse(endDate);
                        if (dateEnd.before(dateInit) || dateEnd.equals(dateInit)) {
                            return false;
                        }
                        return true;
                    } catch (ParseException e) {
                        return false;
                    }
                }
                return true;
            }

            @Override
            public String getMessage(Context context) {
                return getString(R.string.end_date_greater_than_init_date);
            }
        });
        mValidator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        //verifica se existem os tópicos
        long countTopics = TopicCacheManager.getInstance().count(getRealm());

        if (countTopics > 0) {
            organizerTopics();
        } else {
            getTopicsFromServer();
        }
    }

    private void getTopicsFromServer() {
        showLoading();
        Call<TopicsResponse> service = RetrofitManager.getInstance().getTopicService().getTopics(1, 1000);
        service.enqueue(new Callback<TopicsResponse>() {
            @Override
            public void onResponse(Call<TopicsResponse> call, Response<TopicsResponse> response) {
                dismissLoading();
                if (response != null && response.body() != null && !response.body().isError() && response.body().getData() != null
                        && !response.body().getData().isEmpty()) {
                    TopicCacheManager.getInstance().putAll(getRealm(), response.body().getData());
                    submitClick();
                } else {
                    showError();
                }
            }

            @Override
            public void onFailure(Call<TopicsResponse> call, Throwable t) {
                dismissLoading();
                showError(t);
            }
        });
    }

    private void organizerTopics() {
        showLoading();
        PlanningCacheManager.getInstance().deleteAll(getRealm());
        PlanningWeekCacheManager.getInstance().deleteAll(getRealm());

        List<Topic> topics = TopicCacheManager.getInstance().getAll(getRealm());

        Planning planning = new Planning();
        try {
            planning.setInitDate(DateUtil.DATE_DAY_MONTH_YEAR.get().parse(mEdtInitDate.getText().toString()));
            planning.setEndDate(DateUtil.DATE_DAY_MONTH_YEAR.get().parse(mEdtEndDate.getText().toString()));
            int weeks = getWeeks(planning.getInitDate(), planning.getEndDate());
            PlanningWeek[] planningWeeks = new PlanningWeek[weeks];
            for (int i = 0; i < weeks; i++){
                PlanningWeek planningWeek = new PlanningWeek();
                planningWeek.setWeek(i + 1);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(planning.getInitDate());
                calendar.add(Calendar.WEEK_OF_MONTH, i);
                planningWeek.setStart(calendar.getTime());
                planningWeeks[i] = planningWeek;
            }
            for (Topic topic : topics) {
                //topic.setFinalized(false);
                int weekStart = (int) Math.ceil(weeks * topic.getWeekStart() / BuildConfig.NR_WEERS);
                int weekEnd = (int) Math.ceil(weeks * topic.getWeekEnd() / BuildConfig.NR_WEERS);
                for (int i = weekStart; i <= weekEnd; i++) {
                    int arrayIndice = i - 1;
                    if (arrayIndice < 0) {
                        arrayIndice = 0;
                    }
                    PlanningWeek planningWeek = planningWeeks[arrayIndice];
                    if (planningWeek == null) {
                        planningWeek = new PlanningWeek();
                        planningWeek.setWeek(arrayIndice + 1);
                        planningWeeks[arrayIndice] = planningWeek;
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(planning.getInitDate());
                        calendar.add(Calendar.WEEK_OF_MONTH, arrayIndice);
                        planningWeek.setStart(calendar.getTime());
                    }
                    RealmList<Topic> planningWeekTopics = planningWeek.getTopics();
                    if (planningWeekTopics == null) {
                        planningWeekTopics = new RealmList<>();
                    }
                    planningWeekTopics.add(topic);
                    planningWeek.setTopics(planningWeekTopics);
                }
            }

            RealmList<PlanningWeek> planningWeeksRealm = new RealmList<>();
            planning.setPlanningWeek(planningWeeksRealm);

            planningWeeksRealm.addAll(Arrays.asList(planningWeeks));

            PlanningCacheManager.getInstance().put(getRealm(), planning);

            TopicCacheManager.getInstance().putAll(getRealm(), topics);

            dismissLoading();
            if (isAdded()) {
                Navigator.navigateToHomeFragment(getContext());
            }
        } catch (ParseException e) {
            showError();
        }
    }

    private int getWeeks(Date initDate, Date endDate) {
        return DateUtil.getWeeksBetween(initDate, endDate, 1);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isAdded()) {
            String txtError = "";
            for (ValidationError error : errors) {
                View view = error.getView();
                String message = error.getCollatedErrorMessage(getContext());
                txtError += message + "\n";

                if (view.getId() == R.id.edt_init_date) {
                    mImgInitDate.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImageError, null));
                } else if (view.getId() == R.id.edt_end_date) {
                    mImgEndDate.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImageError, null));
                }
            }
            mTxtErros.setText(txtError);
            //mTxtErros.setVisibility(View.VISIBLE);
        }
    }

    private void clearValidation() {
        mTxtErros.setVisibility(View.GONE);
        mTxtErros.setText("");
        mImgInitDate.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImage, null));
        mImgEndDate.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImage, null));
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        Planning planning = PlanningCacheManager.getInstance().get(getRealm(), Planning.ID);
        if (planning == null) {
            return TOOLBAR_TYPE.HIDE_MENU;
        } else {
            return super.getToolbarType();
        }
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.title_change_date);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_change_date;
    }
}
