package br.com.ftd.iralemenem.service.response;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.Expose;

/**
 * Created by ranipieper on 8/3/16.
 */
public class JsonResponse {

    //pode ser data: [] ou data: {}
    @Expose
    private JsonElement data;

    //pode ser error: false ou error: "Mensagem de erro"
    @Expose
    private JsonPrimitive error;

    public JsonElement getData() {
        return data;
    }

    public void setData(JsonElement data) {
        this.data = data;
    }

    public JsonPrimitive getError() {
        return error;
    }

    public void setError(JsonPrimitive error) {
        this.error = error;
    }

}
