package br.com.ftd.iralemenem.service.interfaces;

import br.com.ftd.iralemenem.service.response.TopicResponse;
import br.com.ftd.iralemenem.service.response.TopicsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ranipieper on 7/25/16.
 */
public interface TopicService {

    @GET("studyguide/topic")
    Call<TopicsResponse> getTopics(@Query("filter_value") String discipline, @Query("page") int page,  @Query("limit") int limit);

    @GET("studyguide/topic?filter_type=discipline_id")
    Call<TopicsResponse> getTopics(@Query("page") int page,  @Query("limit") int limit);

    @GET("studyguide/topic/{topic_id}")
    Call<TopicResponse> getTopic(@Path("topic_id") long topicId);
}
