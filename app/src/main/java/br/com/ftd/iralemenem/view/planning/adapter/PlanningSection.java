package br.com.ftd.iralemenem.view.planning.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;

import java.util.List;

import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.cache.TopicCacheManager;
import br.com.ftd.iralemenem.model.Discipline;
import br.com.ftd.iralemenem.model.Topic;
import br.com.ftd.iralemenem.util.ResourcesUtil;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;
import io.realm.Realm;

/**
 * Created by ranieripieper on 8/2/16.
 */
public class PlanningSection extends StatelessSection {

    private Context mContext;
    private Discipline mDiscipline;
    private List<Topic> mTopcis;
    private PlanningAdapterListener mListener;
    private int mWeek;
    int mInitPosition;

    public PlanningSection(Context context, int initPosition, Discipline discipline, List<Topic> topcis, int week, PlanningAdapterListener listener) {
        super(R.layout.row_header_discipline, R.layout.row_topics);
        this.mContext = context;
        this.mDiscipline = discipline;
        this.mTopcis = topcis;
        this.mListener = listener;
        this.mWeek = week;
        this.mInitPosition = initPosition;
    }

    @Override
    public int getContentItemsTotal() {
        return mTopcis.size();
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        // return a custom instance of ViewHolder for the items of this section
        return new TopicViewHolder(view);
    }

    private Topic getTopic(int position) {
        final Topic obj = mTopcis.get(position);
        Realm realm = Realm.getDefaultInstance();
        Topic topicBd = TopicCacheManager.getInstance().get(realm, obj.getId());
        realm.close();
        if (topicBd != null) {
            return topicBd;
        }
        return obj;
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        final TopicViewHolder holder = (TopicViewHolder) viewHolder;
        final Topic obj = getTopic(position);

        holder.mSwipeLayout.close();
        holder.mTxtTopic.setText(obj.getTopic());
        if (obj.getPageStart() == obj.getPageEnd()) {
            holder.mTxtPages.setText(mContext.getString(R.string.unique_page, obj.getPageStart()));
        } else {
            holder.mTxtPages.setText(mContext.getString(R.string.pages, obj.getPageStart(), obj.getPageEnd()));
        }

        changeFinalizedStatus(obj, holder);

        holder.mSwipeLayout.setSwipeEnabled(false);

        holder.mImgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.mSwipeLayout.getOpenStatus().equals(SwipeLayout.Status.Open)) {
                    rotateDots(holder.mImgMenu, 0);
                    holder.mSwipeLayout.close(true);
                } else {
                    rotateDots(holder.mImgMenu, 90);
                    holder.mSwipeLayout.open(true);
                }
            }
        });

        holder.layoutSwipeClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onTopicClicked(obj);
                }
            }
        });

        holder.mImgTopicDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onTopicDoneClicked(obj, position + mInitPosition);
                }
                changeFinalizedStatus(obj, holder);
                rotateDots(holder.mImgMenu, 0);
            }
        });
        rotateDots(holder.mImgMenu, 0);
    }

    private void changeFinalizedStatus(Topic obj, TopicViewHolder holder) {
        if (obj.isFinalized()) {
            //holder.mImgTopicDone.setImageResource(R.drawable.btn_planejamento_marcado);
            holder.layoutSwipeClose.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), R.color.gray3, null));
            holder.mTxtTopic.setTextColor(ResourcesCompat.getColor(mContext.getResources(), R.color.gray4, null));
            holder.mImgMarked.setVisibility(View.VISIBLE);
        } else {
            //holder.mImgTopicDone.setImageResource(R.drawable.btn_planejamento_marcar);
            holder.layoutSwipeClose.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), R.color.gray2, null));
            holder.mTxtTopic.setTextColor(ResourcesCompat.getColor(mContext.getResources(), R.color.grayDark, null));
            holder.mImgMarked.setVisibility(View.GONE);
        }
    }

    private void rotateDots(ImageView imgView, float angle) {
        //AnimatorCompatHelper.clearInterpolator(holder.mImgMenu);
        //ViewCompat.animate(imgView).setDuration(100).rotation(angle);
        ViewCompat.setRotation(imgView, angle);
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        HeaderHolder headerHolder = (HeaderHolder) holder;

        if (mDiscipline == null) {
            headerHolder.mTxtDiscipline.setText("");
        } else {
            headerHolder.mTxtDiscipline.setText(mDiscipline.getDiscipline());
        }

        headerHolder.mTxtAddTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.addTopicClick(mDiscipline.getId(), mWeek);
                }
            }
        });

        headerHolder.mTxtMakeTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.makeTestClick(mDiscipline.getId(), mWeek, mTopcis);
                }
            }
        });

        headerHolder.mImgDiscipline.setImageResource(ResourcesUtil.getDrawableId(String.format("icn_discipline_%d", mDiscipline.getId()), mContext));
        headerHolder.mTxtMakeTest.setPaintFlags(headerHolder.mTxtMakeTest.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }


    public static class TopicViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtTopic;
        public TextView mTxtPages;
        public ImageView mImgMenu;
        public ImageView mImgTopicDone;
        public ImageView mImgMarked;
        public SwipeLayout mSwipeLayout;
        public View layoutSwipeClose;

        public TopicViewHolder(View v) {
            super(v);
            mTxtTopic = (TextView) v.findViewById(R.id.txt_topic);
            mTxtPages = (TextView) v.findViewById(R.id.txt_pages);
            mImgTopicDone = (ImageView) v.findViewById(R.id.img_topic_done);
            mImgMenu = (ImageView) v.findViewById(R.id.img_dots);
            mImgMarked = (ImageView) v.findViewById(R.id.img_marked);
            mSwipeLayout = (SwipeLayout) v.findViewById(R.id.swipe);
            layoutSwipeClose = v.findViewById(R.id.layout_swipe_close);
        }
    }

    //Header
    public static class HeaderHolder extends RecyclerView.ViewHolder {
        public TextView mTxtDiscipline;
        public ImageView mImgDiscipline;
        public TextView mTxtAddTopic;
        public TextView mTxtMakeTest;

        public HeaderHolder(View v) {
            super(v);
            mTxtDiscipline = (TextView) v.findViewById(R.id.txt_discipline);
            mImgDiscipline = (ImageView) v.findViewById(R.id.img_discipline);
            mTxtAddTopic = (TextView) v.findViewById(R.id.txt_add_topic);
            mTxtMakeTest = (TextView) v.findViewById(R.id.txt_make_test);
        }
    }
}
