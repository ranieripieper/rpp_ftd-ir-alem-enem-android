package br.com.ftd.iralemenem.view.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import br.com.ftd.iralemenem.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog progress;
    private Realm mRealm;

    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @Nullable
    @BindView(R.id.app_bar_layout)
    protected AppBarLayout mAppBarLayout;

    @Nullable
    @BindView(R.id.toolbar_title)
    protected TextView mToolbarTitle;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        executeInject();
        initToolbar();
    }

    protected void executeInject() {
        ButterKnife.bind(this);
    }

    public void hideLoadingView() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    public void hideKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }

    /**
     * Inicializa toolbar
     */
    private void initToolbar() {
        if (mToolbar != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mToolbar.setElevation(getResources().getDimensionPixelSize(R.dimen.action_bar_elevation));
            }
            setSupportActionBar(mToolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(false);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                getSupportActionBar().setTitle(getString(R.string.app_name));
            }
        }
    }

    //Toolbar
    public void setToolbarTitle(String s) {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(s);
            mToolbar.setTitle("");
        } else if (mToolbar != null) {
            mToolbar.setTitle(s);
        }
    }

    public void setToolbarTitle(int stringId) {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(stringId);
            mToolbar.setTitle("");
        } else if (mToolbar != null) {
            mToolbar.setTitle(stringId);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    protected Context getContext() {
        return BaseActivity.this;
    }

    @Override
    public void finish() {
        hideKeyboard();
        super.finish();
    }

    protected void TODO() {
        Toast.makeText(getContext(), "TODO", Toast.LENGTH_SHORT).show();
    }

    private void initRealm() {
        if (mRealm == null || mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
    }

    protected Realm getRealm() {
        if (mRealm == null || mRealm.isClosed()) {
            initRealm();
        }
        return mRealm;
    }

    @Override
    protected void onDestroy() {
        if (mRealm != null) {
            mRealm.close();
        }
        super.onDestroy();
    }
}
