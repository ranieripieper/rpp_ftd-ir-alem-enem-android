package br.com.ftd.iralemenem.util;

/**
 * Created by ranieripieper on 8/1/16.
 */
public class NumberUtil {

    public static int getPercent(long parcial, long total) {
        if (total <= 0) {
            return 0;
        }
        int percent = (int)(Math.ceil(((double)parcial/total)*100));
        if (percent == 100 && total != parcial) {
            percent = 99;
        }
        return percent;
    }
}
