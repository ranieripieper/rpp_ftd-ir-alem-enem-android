package br.com.ftd.iralemenem.service.interfaces;

import br.com.ftd.iralemenem.service.response.PerformanceResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ranipieper on 8/4/16.
 */
public interface PerformanceService {

    @GET("studyguide/exam/performance/1")
    Call<PerformanceResponse> getPerformance(@Query("t") String token);
}
