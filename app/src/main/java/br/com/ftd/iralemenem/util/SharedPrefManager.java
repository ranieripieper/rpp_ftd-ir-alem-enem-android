package br.com.ftd.iralemenem.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {

    private static SharedPrefManager sharedPrefManager;
    private Context mContext;

    private static final String PREFERENCES = SharedPrefManager.class + "";

    private static final String TOKEN = "TOKEN";

    private SharedPrefManager(Context context) {
        this.mContext = context;
    }

    public static SharedPrefManager getInstance() {
        return sharedPrefManager;
    }

    public static void init(Application application) {
        sharedPrefManager = new SharedPrefManager(application);
    }

    private SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(PREFERENCES, 0);
    }

    public String getToken() {
        return getSharedPreferences().getString(TOKEN, null);
    }

    public void setToken(String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(TOKEN, value);
        editor.commit();
    }

    private SharedPreferences.Editor getEditor() {
        return getSharedPreferences().edit();
    }

}
