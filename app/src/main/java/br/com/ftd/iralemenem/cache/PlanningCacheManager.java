package br.com.ftd.iralemenem.cache;

import br.com.ftd.iralemenem.cache.base.BaseCache;
import br.com.ftd.iralemenem.model.Planning;
import io.realm.Realm;

/**
 * Created by ranipieper on 7/25/16.
 */
public class PlanningCacheManager extends BaseCache<Planning> {

    private static PlanningCacheManager instance = new PlanningCacheManager();

    private PlanningCacheManager() {
    }

    public static PlanningCacheManager getInstance() {
        return instance;
    }

    public Planning get(Realm realm) {
        return copyFromRealm(realm, realm.where(getReferenceClass()).equalTo(getPrimaryKeyName(), Planning.ID).findFirst());
    }

    @Override
    public Class<Planning> getReferenceClass() {
        return Planning.class;
    }
}
