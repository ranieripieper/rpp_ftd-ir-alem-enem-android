package br.com.ftd.iralemenem.view.account;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import br.com.ftd.iralemenem.BuildConfig;
import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.model.User;
import br.com.ftd.iralemenem.navigation.Navigator;
import br.com.ftd.iralemenem.service.RetrofitManager;
import br.com.ftd.iralemenem.service.response.CreateAccountResponse;
import br.com.ftd.iralemenem.view.base.BaseFragment;
import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 7/4/16.
 */
public class CreateAccountFragment extends BaseFragment {

    @BindView(R.id.txt_erros)
    TextView mTxtErros;

    @NotEmpty(messageResId = R.string.required_field_name)
    @BindView(R.id.edt_name)
    EditText mEdtName;

    @Email(messageResId = R.string.invalid_email)
    @BindView(R.id.edt_email)
    EditText mEdtEmail;

    @Password(min = 6, scheme = Password.Scheme.ANY, messageResId = R.string.invalid_password)
    @BindView(R.id.edt_password)
    EditText mEdtPwd;

    @ConfirmPassword(messageResId = R.string.invalid_conf_password)
    @BindView(R.id.edt_conf_password)
    EditText mEdtConfPwd;

    @BindView(R.id.img_input_name)
    ImageView mImgName;
    @BindView(R.id.img_input_email)
    ImageView mImgEmail;
    @BindView(R.id.img_input_pwd)
    ImageView mImgPwd;
    @BindView(R.id.img_input_conf_pwd)
    ImageView mImgConfPwd;

    public static CreateAccountFragment newInstance() {
        CreateAccountFragment fragment = new CreateAccountFragment();

        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (BuildConfig.DEBUG) {
            mEdtEmail.setText("teste227_@gmail.com");
            mEdtName.setText("Teste");
            mEdtPwd.setText("qwerty1");
            mEdtConfPwd.setText("qwerty1");
        }
    }

    private void callService() {
        showLoading();
        Call<CreateAccountResponse> service = RetrofitManager.getInstance().getUserService().createAccount(mEdtEmail.getText().toString(),
                mEdtPwd.getText().toString(),
                mEdtName.getText().toString());

        service.enqueue(new Callback<CreateAccountResponse>() {
            @Override
            public void onResponse(Call<CreateAccountResponse> call, Response<CreateAccountResponse> response) {
                dismissLoading();
                if (response != null && response.body() != null) {
                    processCreateAccount(response.body());
                } else {
                    showError(response);
                }
            }

            @Override
            public void onFailure(Call<CreateAccountResponse> call, Throwable t) {
                showError(t);
            }
        });
    }

    private void processCreateAccount(CreateAccountResponse response) {
        //tenta fazer o parse de Cadastro com sucesso
        Gson gson = RetrofitManager.getGson();
        try {
            User user = gson.fromJson(response.getData(), User.class);
            if (user != null) {
                showSuccessCreateAccount();
            } else {
                showError();
            }
        } catch (JsonSyntaxException e) {
            //deu algum erro. Tenta recuperar o erro
            if (response.getError().isString()) {
                showLoginError(response.getError().getAsString());
            } else {
                showError();
            }
        }
    }

    private void showLoginError(String error) {
        if (CreateAccountResponse.USER_ALREADY_EXISTS.equalsIgnoreCase(error)) {
            showError(getString(R.string.create_account_user_already_exists));
        } else {
            showError();
        }
    }

    private void showSuccessCreateAccount() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.create_account_success)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Navigator.navigateToLoginFragment(getContext());
                    }
                });

        builder.create().show();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isAdded()) {
            String txtError = "";
            for (ValidationError error : errors) {
                View view = error.getView();
                String message = error.getCollatedErrorMessage(getContext());
                txtError += message + "\n";

                if (view.getId() == R.id.edt_name) {
                    mImgName.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImageError, null));
                } else if (view.getId() == R.id.edt_email) {
                    mImgEmail.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImageError, null));
                } else if (view.getId() == R.id.edt_password) {
                    mImgPwd.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImageError, null));
                } else if (view.getId() == R.id.edt_conf_password) {
                    mImgConfPwd.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImageError, null));
                }
            }
            showError(txtError);
        }
    }


    protected void showError() {
        showError(getString(R.string.generic_error));
    }

    protected void showError(String txtError) {
        if (!isAdded()) {
            return;
        }
        mTxtErros.setText(txtError);
        mTxtErros.setVisibility(View.VISIBLE);
        dismissLoading();
    }

    @Override
    public void onValidationSucceeded() {
        mTxtErros.setVisibility(View.GONE);
        callService();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_create_account;
    }

    @OnClick(R.id.btn_submit)
    void submitClick() {
        clearValidation();
        initValidator();
        mValidator.validate();
    }

    private void clearValidation() {
        mTxtErros.setVisibility(View.GONE);
        mTxtErros.setText("");
        mImgName.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImage, null));
        mImgEmail.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImage, null));
        mImgPwd.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImage, null));
        mImgConfPwd.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.bgInputImage, null));
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.title_sign_up);
    }
}
