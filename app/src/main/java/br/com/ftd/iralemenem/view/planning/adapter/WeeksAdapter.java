package br.com.ftd.iralemenem.view.planning.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import antistatic.spinnerwheel.adapters.AbstractWheelTextAdapter;
import br.com.ftd.iralemenem.R;

/**
 * Created by ranipieper on 7/26/16.
 */
public class WeeksAdapter extends AbstractWheelTextAdapter {

    private int mNrWeeks = 0;

    /**
     * Constructor
     */
    public WeeksAdapter(Context context, int nrWeeks) {
        super(context, R.layout.row_week, NO_RESOURCE);
        mNrWeeks = nrWeeks;
        setItemTextResource(R.id.txt_week);
    }

    @Override
    public View getItem(int index, View cachedView, ViewGroup parent) {
        View view = super.getItem(index, cachedView, parent);

        return view;
    }

    @Override
    public int getItemsCount() {
        return mNrWeeks;
    }

    public int getValue(int index) {
        return index + 1;
    }

    @Override
    protected CharSequence getItemText(int index) {
        return String.format("%02d", index + 1);
    }

}
