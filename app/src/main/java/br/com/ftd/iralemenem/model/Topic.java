package br.com.ftd.iralemenem.model;

import com.google.gson.annotations.Expose;

import br.com.ftd.iralemenem.cache.base.SerializedGsonName;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranipieper on 7/25/16.
 */
public class Topic extends RealmObject {

    /*
        "id": 1,
      "topic": "O estudo da vida",
      "page_start": 5,
      "page_end": 6,
      "week_start": 1,
      "week_end": 1,
      "discipline_id": 3,
      "created_at": "2016-07-18 16:27:37",
      "updated_at": "2016-07-19 09:33:26",
      "deleted_at": null,
      "status": 1,
      "discipline": {
        "id": 3,
        "code": "BIO",
        "discipline": "Biologia"
      }
     */

    private static final long FAKE_ID = -1;

    public static Topic getFake(long disciplineId) {
        Topic topic = new Topic();
        topic.setId(FAKE_ID);
        topic.setDisciplineId(disciplineId);
        return topic;
    }

    public boolean isFake() {
        if (FAKE_ID == this.id) {
            return true;
        }
        return false;
    }

    @PrimaryKey
    @Expose
    private long id;

    @Expose
    private String topic;

    @Expose
    @SerializedGsonName("page_start")
    private int pageStart;

    @Expose
    @SerializedGsonName("page_end")
    private int pageEnd;

    @Expose
    @SerializedGsonName("week_start")
    private int weekStart;

    @Expose
    @SerializedGsonName("week_end")
    private int weekEnd;

    @Expose
    @SerializedGsonName("discipline_id")
    private Long disciplineId;

    @Expose
    private int status;

    @Expose
    private Discipline discipline;

    @Expose
    private RealmList<TopicLink> links;

    @Expose
    private boolean finalized;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getPageStart() {
        return pageStart;
    }

    public void setPageStart(int pageStart) {
        this.pageStart = pageStart;
    }

    public int getPageEnd() {
        return pageEnd;
    }

    public void setPageEnd(int pageEnd) {
        this.pageEnd = pageEnd;
    }

    public int getWeekStart() {
        return weekStart;
    }

    public void setWeekStart(int weekStart) {
        this.weekStart = weekStart;
    }

    public Long getDisciplineId() {
        return disciplineId;
    }

    public void setDisciplineId(Long disciplineId) {
        this.disciplineId = disciplineId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Discipline getDiscipline() {
        return discipline;
    }

    public void setDiscipline(Discipline discipline) {
        this.discipline = discipline;
    }

    public boolean isFinalized() {
        return finalized;
    }

    public void setFinalized(boolean finalized) {
        this.finalized = finalized;
    }

    public int getWeekEnd() {
        return weekEnd;
    }

    public void setWeekEnd(int weekEnd) {
        this.weekEnd = weekEnd;
    }

    /**
     * Gets the links
     *
     * @return links
     */
    public RealmList<TopicLink> getLinks() {
        return links;
    }

    /**
     * Sets the links
     *
     * @param links
     */
    public void setLinks(RealmList<TopicLink> links) {
        this.links = links;
    }
}
