package br.com.ftd.iralemenem.view.planning;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.cache.DisciplineCacheManager;
import br.com.ftd.iralemenem.cache.TopicCacheManager;
import br.com.ftd.iralemenem.model.Discipline;
import br.com.ftd.iralemenem.model.Topic;
import br.com.ftd.iralemenem.model.TopicLink;
import br.com.ftd.iralemenem.service.RetrofitManager;
import br.com.ftd.iralemenem.service.response.TopicResponse;
import br.com.ftd.iralemenem.util.ResourcesUtil;
import br.com.ftd.iralemenem.view.base.BaseFragment;
import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 7/26/16.
 */
public class TopicDetailFragment extends BaseFragment {

    private static String EXTRA_TOPIC_ID = "EXTRA_TOPIC_ID";

    @BindView(R.id.img_discipline)
    ImageView mImgDiscipline;

    @BindView(R.id.txt_discipline)
    TextView mTxtDiscipline;

    @BindView(R.id.txt_topic)
    TextView mTxtTopic;

    @BindView(R.id.txt_pages)
    TextView mTxtPages;

    @BindView(R.id.layout_content)
    LinearLayout mLayoutContent;

    @BindView(R.id.btn_check_finalized)
    Button mBtnCheckFinalized;

    @BindView(R.id.img_checked)
    ImageView mImgChecked;

    private Long mTopicId = null;
    private Topic mTopic = null;

    public static TopicDetailFragment newInstance(long topicId) {
        TopicDetailFragment fragment = new TopicDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(EXTRA_TOPIC_ID, topicId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restore(getArguments());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mTopic == null) {
            return;
        }

        mTxtTopic.setText(mTopic.getTopic());

        Discipline discipline = mTopic.getDiscipline();
        if (discipline == null) {
            discipline = DisciplineCacheManager.getInstance().get(getRealm(), mTopic.getDisciplineId());
        }

        if (discipline == null) {
            mTxtDiscipline.setText("");
        } else {
            mTxtDiscipline.setText(discipline.getDiscipline());
        }

        mImgDiscipline.setImageResource(ResourcesUtil.getDrawableId(String.format("icn_discipline_%d", mTopic.getDisciplineId()), getContext()));

        if (mTopic.getPageStart() != mTopic.getPageEnd()) {
            mTxtPages.setText(getString(R.string.location_pages, mTopic.getPageStart(), mTopic.getPageEnd()));
        } else {
            mTxtPages.setText(getString(R.string.location_unique_page, mTopic.getPageStart()));
        }

        adjustViewFinalized(mTopic.isFinalized());

        if (mTopic.getLinks() != null && !mTopic.getLinks().isEmpty()) {
            renderLinks(mTopic.getLinks());
        } else {
            getTopicLinks();
        }
    }

    private void adjustViewFinalized(boolean finalized) {
        if (finalized) {
            mImgChecked.setVisibility(View.VISIBLE);
            mBtnCheckFinalized.setText(R.string.checked_unfinalized);
        } else {
            mImgChecked.setVisibility(View.GONE);
            mBtnCheckFinalized.setText(R.string.checked_finalized);
        }
    }

    @OnClick(R.id.btn_check_finalized)
    void btnCheckFinalizedClick() {
        mTopic = TopicCacheManager.getInstance().get(getRealm(), mTopicId);
        mTopic.setFinalized(!mTopic.isFinalized());
        mTopic = TopicCacheManager.getInstance().put(getRealm(), mTopic);
        adjustViewFinalized(mTopic.isFinalized());
    }

    private void getTopicLinks() {
        showLoading();
        Call<TopicResponse> service = RetrofitManager.getInstance().getTopicService().getTopic(mTopicId);
        service.enqueue(new Callback<TopicResponse>() {
            @Override
            public void onResponse(Call<TopicResponse> call, Response<TopicResponse> response) {
                if (response.body() != null && response.body().getData() != null) {
                    TopicCacheManager.getInstance().updateWithoutNullsAllFields(getRealm(), response.body().getData());
                    renderLinks(response.body().getData().getLinks());
                } else {
                    showError();
                }
                dismissLoading();
            }

            @Override
            public void onFailure(Call<TopicResponse> call, Throwable t) {
                showError();
            }
        });
    }

    private void renderLinks(List<TopicLink> links) {
        if (links != null && !links.isEmpty()) {
            for (TopicLink link : links) {
                addLink(link);
            }
            mLayoutContent.setVisibility(View.VISIBLE);
        }
    }

    private void addLink(final TopicLink link) {
        if (!isAdded()) {
            return;
        }
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View v = inflater.inflate(R.layout.row_link, null);
        TextView txtLink = (TextView)v.findViewById(R.id.txt_link);
        txtLink.setText(link.getDescription());
        txtLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = link.getLink();
                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
        if (isAdded()) {
            mLayoutContent.addView(v);
        }

    }

    @Override
    public void onRestoreState(Bundle savedInstanceState) {
        super.onRestoreState(savedInstanceState);
        if (mTopic == null) {
            restore(getArguments());
        }
    }

    private void restore(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mTopicId = savedInstanceState.getLong(EXTRA_TOPIC_ID);
            mTopic = TopicCacheManager.getInstance().get(getRealm(), mTopicId);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_topic_detail;
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.title_planning);
    }
}
