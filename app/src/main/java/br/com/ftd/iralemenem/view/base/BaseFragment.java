package br.com.ftd.iralemenem.view.base;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.util.DateUtil;
import br.com.ftd.iralemenem.view.MainActivity;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Response;

/**
 * Created by ranipieper on 11/11/15.
 */
public abstract class BaseFragment extends Fragment implements Validator.ValidationListener {

    private ProgressDialog progress;
    private Realm mRealm;


    protected Validator mValidator;

    protected abstract int getLayoutResource();

    private static String TAG = "BaseFragment";

    public enum TOOLBAR_TYPE {
        DRAWER,
        BACK_BUTTON,
        CUSTOM_BACK_BUTTON,
        NOTHING,
        INVISIBLE,
        HIDE_MENU
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Closes keyboard on fragment first appearance.
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState == null) {
            onRestoreState(getArguments());
        } else {
            onRestoreState(savedInstanceState);
        }

        configToolbar();
        hideKeyboard();
    }

    protected void setImageAndTextToolbar() {
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        executeInject(view);
        return view;
    }

    protected void executeInject(View view) {
        ButterKnife.bind(this, view);
    }

    public void onResumeFromBackStack() {
        configToolbar();
        hideKeyboard();
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hideKeyboard();
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isAdded()) {
            configToolbar();
        }
    }

    /**
     * Used by Leak Canary
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void hideKeyboard() {
        if (isAdded() && getView() != null) {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

    protected void configToolbar() {
        if (needConfigToolbar() && isAdded()) {
            setToolbarTitle(getToolbarTitle());
            if (TOOLBAR_TYPE.BACK_BUTTON.equals(getToolbarType())) {
                setNavBackMode();
            } else if (TOOLBAR_TYPE.DRAWER.equals(getToolbarType())) {
                resetBackMode();
            } else if (TOOLBAR_TYPE.NOTHING.equals(getToolbarType())) {
                hideDrawerAndBackButton();
            } else if (TOOLBAR_TYPE.CUSTOM_BACK_BUTTON.equals(getToolbarType())) {
                setCustomNavBackMode();
            } else if (TOOLBAR_TYPE.INVISIBLE.equals(getToolbarType())) {
                setToolbarInvisible();
            } else if (TOOLBAR_TYPE.HIDE_MENU.equals(getToolbarType())) {
                hiddeMenu();
            }
            setImageAndTextToolbar();
        }
    }

    protected boolean needConfigToolbar() {
        return true;
    }

    /*
     Call back when user presses back button or back on the navigation.
    */
    protected void onBackPressed() {
        hideKeyboard();
    }

    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.NOTHING;
    }

    protected String getToolbarTitle() {
        return "";
    }

    public void setNavBackMode() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).setNavBackMode();
        }
    }

    public void hiddeMenu() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((MainActivity) getActivity()).hideMenu();
        }
    }

    public void setToolbarInvisible() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).setToolbarInvisible();
        }
    }

    public void setCustomNavBackMode() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).setNavBackMode(getCustomDrawableBackMode(), getCustomDrawableBackModeListener());
        }
    }

    public void resetBackMode() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).resetBackMode();
        }
    }

    public void hideDrawerAndBackButton() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).hideDrawerAndBackButton();
        }
    }

    public void setToolbarTitle(String s) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).setToolbarTitle(s);
        }
    }

    public void setToolbarTitle(int stringId) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).setToolbarTitle(stringId);
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (clearMenu()) {
            menu.clear();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    protected boolean clearMenu() {
        return true;
    }

    protected void finish() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    protected Drawable getCustomDrawableBackMode() {
        return ((AppCompatActivity) getActivity()).getDrawerToggleDelegate().getThemeUpIndicator();
    }

    protected View.OnClickListener getCustomDrawableBackModeListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        };
    }

    protected void TODO() {
        Toast.makeText(getActivity(), "TODO", Toast.LENGTH_SHORT).show();
    }

    protected void TODO(String msg) {
        Toast.makeText(getActivity(), "TODO " + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onValidationSucceeded() {
        if (isAdded()) {
            Toast.makeText(getContext(), ">>>> Implementar @Override onValidationSucceeded", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        if (isAdded()) {
            for (ValidationError error : errors) {
                View view = error.getView();
                String message = error.getCollatedErrorMessage(getContext());

                // Display error messages ;)
                if (view instanceof EditText) {
                    ((EditText) view).setError(message);
                } else {
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    protected boolean backButtonNavigateToPreviousFragment() {
        return true;
    }


    protected void initValidator() {
        if (mValidator == null) {
            mValidator = new Validator(this);
            mValidator.setValidationListener(this);
        }
    }

    public DatePickerDialog buildDatePicker(final TextView txtDateField, Date maxDate, Date minDate, Date preSelectDate) {
        return buildDatePicker(txtDateField, maxDate, minDate, preSelectDate, null);
    }

    public DatePickerDialog buildDatePicker(final TextView txtDateField, Date maxDate, Date minDate, Date preSelectDate, final DatePickerDialog.OnDateSetListener datePickerListener) {

        Context context = getContext();
        DatePickerDialog datePicker;

        DatePickerDialog.OnDateSetListener datePickerListenerSetText = new DatePickerDialog.OnDateSetListener() {

            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                String year1 = String.valueOf(selectedYear);
                String month1 = String.format("%02d",  selectedMonth + 1);
                String day1 = String.format("%02d", selectedDay);
                if (txtDateField != null && isAdded()) {
                    txtDateField.setText(day1 + "/" + month1 + "/" + year1);
                }

                if (datePickerListener != null) {
                    datePickerListener.onDateSet(view, selectedYear, selectedMonth, selectedDay);
                }

            }
        };

        Calendar cal = Calendar.getInstance(TimeZone.getDefault());

        if (!TextUtils.isEmpty(txtDateField.getText().toString())) {
            try {
                Date dt = DateUtil.DATE_DAY_MONTH_YEAR.get().parse(txtDateField.getText().toString());
                cal.setTime(dt);
            } catch (Exception e) {
            }
        } else if (preSelectDate != null) {
            cal.setTime(preSelectDate);
        } else {
            cal.setTime(new Date());
        }

        if (isBrokenSamsungDevice()) {
            context = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog);
        }
        datePicker = new DatePickerDialog(context,
                R.style.DialogTheme,
                datePickerListenerSetText,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

        if (isBrokenSamsungDevice()) {
            datePicker.setTitle("");
            datePicker.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        }

        datePicker.setCancelable(false);
        datePicker.getDatePicker().setCalendarViewShown(false);
        datePicker.getDatePicker().setSpinnersShown(true);

        if (maxDate != null) {
            Calendar calMaxDate = Calendar.getInstance();
            calMaxDate.setTime(maxDate);
            calMaxDate.set(Calendar.HOUR_OF_DAY, 23);
            calMaxDate.set(Calendar.MINUTE, 59);
            calMaxDate.set(Calendar.SECOND, 59);
            datePicker.getDatePicker().setMaxDate(calMaxDate.getTimeInMillis());
        }

        if (minDate != null) {
            Calendar calMinDate = Calendar.getInstance();
            calMinDate.setTime(minDate);
            calMinDate.set(Calendar.HOUR_OF_DAY, 0);
            calMinDate.set(Calendar.MINUTE, 0);
            calMinDate.set(Calendar.SECOND, 0);
            datePicker.getDatePicker().setMinDate(calMinDate.getTimeInMillis());
        }
        return datePicker;
    }

    private static boolean isBrokenSamsungDevice() {
        return (Build.MANUFACTURER.equalsIgnoreCase("samsung")
                && isBetweenAndroidVersions(
                Build.VERSION_CODES.LOLLIPOP,
                Build.VERSION_CODES.LOLLIPOP_MR1));
    }

    private static boolean isBetweenAndroidVersions(int min, int max) {
        return Build.VERSION.SDK_INT >= min && Build.VERSION.SDK_INT <= max;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void onRestoreState(Bundle savedInstanceState) {
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mRealm != null) {
            mRealm.close();
        }
    }

    private void initRealm() {
        if (mRealm == null || mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
    }

    protected Realm getRealm() {
        if (mRealm == null || mRealm.isClosed()) {
            initRealm();
        }
        return mRealm;
    }

    protected void showLoading() {
        dismissLoading();
        if (isAdded()) {
            progress = getProgressDialog();
            progress.show();
        }
    }

    protected void dismissLoading() {
        if (progress != null && progress.isShowing() && isAdded()) {
            progress.dismiss();
            progress = null;
        }
    }

    private ProgressDialog getProgressDialog() {
        ProgressDialog progress = ProgressDialog.show(getActivity(), null, null, true, false);
        progress.setContentView(R.layout.loading);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            progress.getWindow().setDimAmount(0);
        }
        return progress;
    }

    protected void showError() {
        showError(getString(R.string.generic_error));
    }

    protected void showError(int res) {
        showError(getString(res));
    }

    protected void showError(Throwable t) {
        showError(getString(R.string.generic_error));
    }

    protected void showError(Response response) {
        showError(getString(R.string.generic_error));
    }

    protected void showError(String error) {
        dismissLoading();
        if (isAdded()) {
            if (TextUtils.isEmpty(error)) {
                Toast.makeText(getContext(), R.string.generic_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            }

        }
    }
}
