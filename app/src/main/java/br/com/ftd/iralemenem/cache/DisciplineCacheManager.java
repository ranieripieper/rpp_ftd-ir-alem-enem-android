package br.com.ftd.iralemenem.cache;

import br.com.ftd.iralemenem.cache.base.BaseCache;
import br.com.ftd.iralemenem.model.Discipline;

/**
 * Created by ranipieper on 7/25/16.
 */
public class DisciplineCacheManager extends BaseCache<Discipline> {

    private static DisciplineCacheManager instance = new DisciplineCacheManager();

    private DisciplineCacheManager() {
    }

    public static DisciplineCacheManager getInstance() {
        return instance;
    }

    @Override
    public Class<Discipline> getReferenceClass() {
        return Discipline.class;
    }
}
