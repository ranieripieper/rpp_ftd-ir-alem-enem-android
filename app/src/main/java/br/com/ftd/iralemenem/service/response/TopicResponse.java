package br.com.ftd.iralemenem.service.response;

import com.google.gson.annotations.Expose;

import br.com.ftd.iralemenem.model.Topic;

/**
 * Created by ranipieper on 7/25/16.
 */
public class TopicResponse {

    @Expose
    private Topic data;

    /**
     * Gets the data
     *
     * @return data
     */
    public Topic getData() {
        return data;
    }

    /**
     * Sets the data
     *
     * @param data
     */
    public void setData(Topic data) {
        this.data = data;
    }
}
