package br.com.ftd.iralemenem.view.custom;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by ranieripieper on 8/2/16.
 */
public class WheelVerticalView extends antistatic.spinnerwheel.WheelVerticalView {

    public WheelVerticalView(Context context) {
        this(context, (AttributeSet) null);
    }

    public WheelVerticalView(Context context, AttributeSet attrs) {
        this(context, attrs, antistatic.spinnerwheel.R.attr.abstractWheelViewStyle);
    }

    public WheelVerticalView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setSelectorPaintCoeff(float coeff) {
        coeff = 0.7f;
        super.setSelectorPaintCoeff(coeff);
    }

}
