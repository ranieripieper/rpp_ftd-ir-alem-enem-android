package br.com.ftd.iralemenem.cache.base;

import android.content.Context;

import br.com.ftd.iralemenem.BuildConfig;
import br.com.ftd.iralemenem.model.Discipline;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by ranipieper on 7/25/16.
 */
public class RealmManager {

    private static final long REALMVERSION = 1;
    private static RealmConfiguration mConfig;

    private RealmManager() {
    }

    public static void init(final Context context) {
        Realm.Transaction initialData = new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //realm.beginTransaction();
                insertInitData(context, realm);
                //realm.commitTransaction();
            }
        };

        RealmConfiguration.Builder builder = new RealmConfiguration.Builder(context)
                .schemaVersion(REALMVERSION)
                .initialData(initialData).name("ir_alem_enem.db");

        if (BuildConfig.DEBUG) {
            builder.deleteRealmIfMigrationNeeded();
        }
        mConfig = builder.build();
        Realm.setDefaultConfiguration(mConfig);
    }

    private static void insertInitData(Context context, Realm realm) {
        /*
            Biologia - 3
            Química - 29
            Física - 13
            Matemática - 23
            Geografia - 15
            História - 17
            Filosofia - 12
            Sociologia - 31
            Arte - 2
            Educação Física - 8
            Espanhol - 11
            Inglês - 18
            Literatura - 20
            Gramática - 49
            Produção de Texto - 28
         */
        realm.copyToRealmOrUpdate(new Discipline(3, "Biologia"));
        realm.copyToRealmOrUpdate(new Discipline(29, "Química"));
        realm.copyToRealmOrUpdate(new Discipline(13, "Física"));
        realm.copyToRealmOrUpdate(new Discipline(23, "Matemática"));
        realm.copyToRealmOrUpdate(new Discipline(15, "Geografia"));
        realm.copyToRealmOrUpdate(new Discipline(17, "História"));
        realm.copyToRealmOrUpdate(new Discipline(12, "Filosofia"));
        realm.copyToRealmOrUpdate(new Discipline(31, "Sociologia"));
        realm.copyToRealmOrUpdate(new Discipline(2, "Arte"));
        realm.copyToRealmOrUpdate(new Discipline(8, "Educação Física"));
        realm.copyToRealmOrUpdate(new Discipline(11, "Espanhol"));
        realm.copyToRealmOrUpdate(new Discipline(18, "Inglês"));
        realm.copyToRealmOrUpdate(new Discipline(20, "Literatura"));
        realm.copyToRealmOrUpdate(new Discipline(49, "Gramática"));
        realm.copyToRealmOrUpdate(new Discipline(28, "Produção de Texto"));
    }
}
