package br.com.ftd.iralemenem.view;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.cache.PlanningCacheManager;
import br.com.ftd.iralemenem.model.Planning;
import br.com.ftd.iralemenem.navigation.Navigator;
import br.com.ftd.iralemenem.util.SharedPrefManager;
import br.com.ftd.iralemenem.view.base.BaseFragmentActivity;
import butterknife.BindView;

public class MainActivity extends BaseFragmentActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.navigation_view)
    protected NavigationView mNavigationView;

    @BindView(R.id.drawer_layout)
    protected DrawerLayout mDrawerLayout;

    protected boolean hideMenu = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationView.setNavigationItemSelectedListener(this);

        mNavigationView.getHeaderView(0).findViewById(R.id.img_close_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawers();
            }
        });
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        hideDrawerAndBackButton();

        Planning planning = PlanningCacheManager.getInstance().get(getRealm());
        if (planning == null) {
            Navigator.navigateToChangeDateFragment(getContext());
        } else {
            Navigator.navigateToHomeFragment(getContext());
        }
    }

    @Override
    protected int getLayoutFragmentContainer() {
        return R.id.layout_fragment_container;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!hideMenu) {
            getMenuInflater().inflate(R.menu.activity_main, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.ic_menu) {
            invalidateMenu();
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.openDrawer(GravityCompat.END);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.ic_menu_sair) {
            SharedPrefManager.getInstance().setToken("");
            Navigator.navigateToHomeFragment(getContext());
        } else if (id == R.id.ic_practice_test) {
            Navigator.navigateToPracticeTestProgressFragment(getContext());
        } else if (id == R.id.ic_meu_progress) {
            Navigator.navigateToStudyProgressFragment(getContext());
        } else if (id == R.id.ic_menu_change_date) {
            Navigator.navigateToChangeDateFragment(getContext());
        } else if (id == R.id.ic_menu_home) {
            Navigator.navigateToHomeFragment(getContext());
        } else if (id == R.id.ic_menu_planning) {
            Navigator.navigateToPlanningFragment(getContext());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    private void invalidateMenu() {
        if (TextUtils.isEmpty(SharedPrefManager.getInstance().getToken())) {
            mNavigationView.getMenu().setGroupVisible(R.id.group_exit, false);
        } else {
            mNavigationView.getMenu().setGroupVisible(R.id.group_exit, true);
        }
    }

    @Override
    public void resetBackMode() {
        super.resetBackMode();
        showMenu();
    }

    @Override
    public void hideDrawerAndBackButton() {
        super.hideDrawerAndBackButton();
        showMenu();
    }

    @Override
    public void setToolbarInvisible() {
        super.setToolbarInvisible();
        hideMenu();
    }

    @Override
    public void setNavBackMode() {
        super.setNavBackMode();
        showMenu();
    }

    private void closeDrawers() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawers();
        }
    }

    public void hideMenu() {
        hideMenu = true;
        invalidateOptionsMenu();
    }

    public void showMenu() {
        hideMenu = false;
        invalidateOptionsMenu();
    }
}
