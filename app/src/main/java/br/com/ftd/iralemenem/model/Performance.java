package br.com.ftd.iralemenem.model;

import com.google.gson.annotations.Expose;

import java.util.List;

import br.com.ftd.iralemenem.cache.base.SerializedGsonName;

/**
 * Created by ranipieper on 7/4/16.
 */
public class Performance {

    @Expose
    @SerializedGsonName("id")
    private long id;

    @Expose
    @SerializedGsonName("code")
    private String code;

    @Expose
    @SerializedGsonName("discipline")
    private String discipline;

    @Expose
    @SerializedGsonName("exams_performance")
    private List<ExamsPerformance> examsPerformance;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public List<ExamsPerformance> getExamsPerformance() {
        return examsPerformance;
    }

    public void setExamsPerformance(List<ExamsPerformance> examsPerformance) {
        this.examsPerformance = examsPerformance;
    }
}
