package br.com.ftd.iralemenem.service.response;

import android.text.TextUtils;

import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.Expose;

import java.util.List;

import br.com.ftd.iralemenem.cache.base.SerializedGsonName;

/**
 * Created by ranipieper on 7/25/16.
 */
public class BaseListResponse<T> {

    @Expose
    private List<T> data;

    //pode ser error: false ou error: "Mensagem de erro"
    @Expose
    @SerializedGsonName("error")
    private JsonPrimitive jsonError;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public JsonPrimitive getJsonError() {
        return jsonError;
    }

    public void setJsonError(JsonPrimitive jsonError) {
        this.jsonError = jsonError;
    }

    public boolean isError() {
        if (jsonError != null) {
            if (jsonError.isString() && !TextUtils.isEmpty(jsonError.getAsString())) {
                return true;
            } else if (jsonError.isBoolean() && jsonError.getAsBoolean()) {
                return true;
            }
        }

        return false;
    }

    public String getError() {
        if (jsonError.isString() && !TextUtils.isEmpty(jsonError.getAsString())) {
            return jsonError.getAsString();
        }
        return null;
    }
}
