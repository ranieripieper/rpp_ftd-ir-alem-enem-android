package br.com.ftd.iralemenem.model;

import com.google.gson.annotations.Expose;

import br.com.ftd.iralemenem.cache.base.SerializedGsonName;

/**
 * Created by ranipieper on 8/4/16.
 */
public class ExamsPerformance {

    @Expose
    @SerializedGsonName("total_anwsers")
    private long totalAnwsers;

    @Expose
    @SerializedGsonName("correct_answers")
    private long correctAnswers;

    @Expose
    @SerializedGsonName("discipline_id")
    private long disciplineId;

    public long getTotalAnwsers() {
        return totalAnwsers;
    }

    public void setTotalAnwsers(long totalAnwsers) {
        this.totalAnwsers = totalAnwsers;
    }

    public long getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(long correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public long getDisciplineId() {
        return disciplineId;
    }

    public void setDisciplineId(long disciplineId) {
        this.disciplineId = disciplineId;
    }
}
