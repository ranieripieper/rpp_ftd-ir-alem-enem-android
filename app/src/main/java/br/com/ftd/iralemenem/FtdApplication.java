package br.com.ftd.iralemenem;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;

import br.com.ftd.iralemenem.cache.base.RealmManager;
import br.com.ftd.iralemenem.service.RetrofitManager;
import br.com.ftd.iralemenem.util.SharedPrefManager;
import io.fabric.sdk.android.Fabric;

public class FtdApplication extends MultiDexApplication {


    @Override
    public void onCreate() {
        super.onCreate();

        //inicia crashlytics
        initCrashlytics();

        SharedPrefManager.getInstance().init(this);

        initCache();

        initRetrofitManager();

    }

    protected void initCrashlytics() {
        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder()
                        .disabled(BuildConfig.DEBUG)
                        .build())
                .build();
        // Initialize Fabric with the debug-disabled crashlytics.
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, crashlyticsKit, new Answers(), new Crashlytics());
        } else {
            Fabric.with(this, crashlyticsKit);
        }
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void initCache() {
        RealmManager.init(this);
    }

    private void initRetrofitManager() {
        RetrofitManager.getInstance().initialize();
    }
}
