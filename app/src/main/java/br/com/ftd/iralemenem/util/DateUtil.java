package br.com.ftd.iralemenem.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by ranipieper on 11/12/15.
 */
public class DateUtil {

    public static final ThreadLocal<DateFormat> DATE_YEAR_MONTH_DAY = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_DAY_MONTH_YEAR = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("dd/MM/yyyy");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_DAY_MONTH = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("dd/MM");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_MONTH_CHART = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("MMM");
        }
    };

    public static final long getDiffDays(Date start, Date end) {
        if ((start == null || end == null) || (start.after(end)) ) {
            return 0;
        }

        start = resetTime(start);
        end = resetTime(end);

        return ((end.getTime() - start.getTime()) / 1000L / 60L / 60L / 24L);
    }


    public static int getWeeksBetween(Date a, Date b, int init) {

        if (b.before(a)) {
            return -getWeeksBetween(b, a, init);
        }
        a = resetTime(a);
        b = resetTime(b);

        Calendar cal = new GregorianCalendar();
        cal.setTime(a);
        int weeks = init;
        while (cal.getTime().before(b)) {
            // add another week
            cal.add(Calendar.WEEK_OF_YEAR, 1);
            weeks++;
        }
        return weeks;
    }

    public static Date resetTime(Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
