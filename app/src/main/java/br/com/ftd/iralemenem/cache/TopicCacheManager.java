package br.com.ftd.iralemenem.cache;

import java.util.List;

import br.com.ftd.iralemenem.cache.base.BaseCache;
import br.com.ftd.iralemenem.model.Topic;
import io.realm.Realm;

/**
 * Created by ranipieper on 7/25/16.
 */
public class TopicCacheManager extends BaseCache<Topic> {

    private static TopicCacheManager instance = new TopicCacheManager();

    private TopicCacheManager() {
    }

    public static TopicCacheManager getInstance() {
        return instance;
    }

    public long countFinalized(Realm realm) {
        return realm.where(getReferenceClass()).equalTo("finalized", true).count();
    }

    public long countFinalized(Realm realm, long disciplineId) {
        return realm.where(getReferenceClass()).equalTo("finalized", true).equalTo("disciplineId", disciplineId).count();
    }

    public List<Topic> getAll(Realm realm, long disciplineId) {
        return copyFromRealm(realm, realm.where(getReferenceClass()).equalTo("disciplineId", disciplineId).findAll());
    }

    public long count(Realm realm, long disciplineId) {
        return realm.where(getReferenceClass()).equalTo("disciplineId", disciplineId).count();
    }

    @Override
    public Class<Topic> getReferenceClass() {
        return Topic.class;
    }
}
