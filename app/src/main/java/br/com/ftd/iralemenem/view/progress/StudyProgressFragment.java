package br.com.ftd.iralemenem.view.progress;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.cache.DisciplineCacheManager;
import br.com.ftd.iralemenem.cache.TopicCacheManager;
import br.com.ftd.iralemenem.model.Discipline;
import br.com.ftd.iralemenem.util.NumberUtil;
import br.com.ftd.iralemenem.view.base.BaseFragment;
import br.com.ftd.iralemenem.view.custom.DividerDecoration;
import br.com.ftd.iralemenem.view.progress.adapter.StudyDisciplineAdapter;
import butterknife.BindView;

/**
 * Created by ranipieper on 7/4/16.
 */
public class StudyProgressFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.txt_total_progress)
    TextView mTxtTotalProgress;

    @BindView(R.id.txt_header)
    TextView mTxtHeader;

    @BindView(R.id.view_indicator)
    View mViewIndicator;

    public static StudyProgressFragment newInstance() {
        StudyProgressFragment fragment = new StudyProgressFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mTxtHeader.setText(R.string.progress_header_text);
        List<Discipline> lst = DisciplineCacheManager.getInstance().getAll(getRealm());

        long total = 0;
        long totalFinalized = 0;
        for (Discipline discipline : lst) {
            long countFinalized = TopicCacheManager.getInstance().countFinalized(getRealm(), discipline.getId());
            long count = TopicCacheManager.getInstance().count(getRealm(), discipline.getId());
            int percent = NumberUtil.getPercent(countFinalized, count);
            discipline.setPercent(percent);

            total += count;
            totalFinalized += countFinalized;
        }

        int percentTotal = NumberUtil.getPercent(totalFinalized, total);
        adjustPercentView(percentTotal);

        mRecyclerView.setAdapter(new StudyDisciplineAdapter(getContext(), lst));
        mRecyclerView.addItemDecoration(DividerDecoration.getDefaultDividerDecoration(getContext()));

    }

    private void adjustPercentView(int percentTotal) {
        mTxtTotalProgress.setText(getString(R.string.txt_percent, percentTotal));
        mTxtTotalProgress.setVisibility(View.VISIBLE);
        mViewIndicator.setVisibility(View.VISIBLE);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        width -= getResources().getDimensionPixelSize(R.dimen.margin_right_progress);
        width -= getResources().getDimensionPixelSize(R.dimen.margin_left_progress);

        int x = width * percentTotal / 100;
        mTxtTotalProgress.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        ViewCompat.animate(mTxtTotalProgress).translationX(getResources().getDimensionPixelSize(R.dimen.margin_left_progress) + x - mTxtTotalProgress.getMeasuredWidth()/2).start();
        ViewCompat.animate(mViewIndicator).translationX(getResources().getDimensionPixelSize(R.dimen.margin_left_progress) + x).start();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_progress;
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.title_my_studies_progress);
    }
}
