package br.com.ftd.iralemenem.view;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.navigation.Navigator;
import br.com.ftd.iralemenem.view.base.BaseActivity;

/**
 * Created by ranieripieper on 8/2/16.
 */
public class DeepLinkActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deeplink);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Navigator.navigateToMain(DeepLinkActivity.this);
                finish();
            }
        }, 100);

    }
}
