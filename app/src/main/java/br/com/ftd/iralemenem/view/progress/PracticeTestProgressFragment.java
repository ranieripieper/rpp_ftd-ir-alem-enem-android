package br.com.ftd.iralemenem.view.progress;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.List;

import br.com.ftd.iralemenem.R;
import br.com.ftd.iralemenem.model.Performance;
import br.com.ftd.iralemenem.navigation.Navigator;
import br.com.ftd.iralemenem.service.RetrofitManager;
import br.com.ftd.iralemenem.service.response.PerformanceResponse;
import br.com.ftd.iralemenem.util.SharedPrefManager;
import br.com.ftd.iralemenem.view.base.BaseFragment;
import br.com.ftd.iralemenem.view.custom.DividerDecoration;
import br.com.ftd.iralemenem.view.progress.adapter.PracticeTestAdapter;
import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ranipieper on 7/4/16.
 */
public class PracticeTestProgressFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    public static PracticeTestProgressFragment newInstance() {
        PracticeTestProgressFragment fragment = new PracticeTestProgressFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (TextUtils.isEmpty(SharedPrefManager.getInstance().getToken())) {
            needLogin();
        } else {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);

            mRecyclerView.addItemDecoration(DividerDecoration.getDefaultDividerDecoration(getContext()));
            callService();
        }
    }

    private void needLogin() {
        SharedPrefManager.getInstance().setToken("");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.performance_need_login)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Navigator.navigateToLoginFragment(getContext());
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        builder.create().show();
    }

    private void callService() {
        showLoading();
        Call<PerformanceResponse> service = RetrofitManager.getInstance().getPerformanceService().getPerformance(SharedPrefManager.getInstance().getToken());
        service.enqueue(new Callback<PerformanceResponse>() {
            @Override
            public void onResponse(Call<PerformanceResponse> call, Response<PerformanceResponse> response) {
                dismissLoading();
                if (response != null && response.body() != null) {
                    processResult(response.body());
                } else {
                    showError(response);
                }
            }

            @Override
            public void onFailure(Call<PerformanceResponse> call, Throwable t) {
                showError();
            }
        });
    }

    protected void showError(Response response) {
        dismissLoading();
        if (response != null && response.errorBody() != null) {
            try {
                String errorBody = response.errorBody().string();
                PerformanceResponse performanceResponse = RetrofitManager.getGson().fromJson(errorBody, PerformanceResponse.class);
                if (performanceResponse != null && performanceResponse.isError()) {
                    if (PerformanceResponse.INVALID_SESSION.equalsIgnoreCase(performanceResponse.getError())) {
                        showError(R.string.error_invalid_session);
                    }
                }
                needLogin();
            } catch (IOException e){
                needLogin();
            } catch (JsonSyntaxException e) {
                needLogin();
            }
        }
    }

    private void processResult(PerformanceResponse response) {
        if (response != null && response.isError()) {
            showError(response.getError());
        } else {
            //show eresponses
            renderPracticeTest(response.getData());
        }
    }

    private void renderPracticeTest(List<Performance> lst) {
        mRecyclerView.setAdapter(new PracticeTestAdapter(getActivity(), lst));
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_progress;
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.title_practice_test_progress);
    }
}
