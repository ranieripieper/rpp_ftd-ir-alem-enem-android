package br.com.ftd.iralemenem.service.response;

/**
 * Created by ranipieper on 8/3/16.
 */
public class LoginResponse extends JsonResponse {

    public static final String INVALID_PASSWORD = "invalid-password";
    public static final String USER_NOT_FOUND = "user-not-found";
}
