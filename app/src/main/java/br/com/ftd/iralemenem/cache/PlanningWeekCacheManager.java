package br.com.ftd.iralemenem.cache;

import br.com.ftd.iralemenem.cache.base.BaseCache;
import br.com.ftd.iralemenem.model.PlanningWeek;

/**
 * Created by ranipieper on 7/25/16.
 */
public class PlanningWeekCacheManager extends BaseCache<PlanningWeek> {

    private static PlanningWeekCacheManager instance = new PlanningWeekCacheManager();

    private PlanningWeekCacheManager() {
    }

    public static PlanningWeekCacheManager getInstance() {
        return instance;
    }

    @Override
    public String getPrimaryKeyName() {
        return "week";
    }

    @Override
    public Class<PlanningWeek> getReferenceClass() {
        return PlanningWeek.class;
    }
}
