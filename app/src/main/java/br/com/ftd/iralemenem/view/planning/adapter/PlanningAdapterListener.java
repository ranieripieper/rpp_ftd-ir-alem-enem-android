package br.com.ftd.iralemenem.view.planning.adapter;

import java.util.List;

import br.com.ftd.iralemenem.model.Topic;

/**
 * Created by ranieripieper on 8/2/16.
 */
public interface PlanningAdapterListener {

    void onTopicClicked(Topic topic);
    void onTopicDoneClicked(Topic topic, int position);
    void addTopicClick(long disciplineId, int week);
    void makeTestClick(long disciplineId, int week, List<Topic> topics);
}
